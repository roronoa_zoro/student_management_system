package com.zeroosoft.education.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="day_setup")
public class DaySetupEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "day_id")
	private Integer dayId;
	
	@Column(name = "day_name")
	private String dayName;

	public Integer getDayId() {
		return dayId;
	}

	public void setDayId(Integer dayId) {
		this.dayId = dayId;
	}

	public String getDayName() {
		return dayName;
	}

	public void setDayName(String dayName) {
		this.dayName = dayName;
	}

	public DaySetupEntity(String dayName) {
		super();
		this.dayName = dayName;
	}

	public DaySetupEntity(Integer dayId, String dayName) {
		super();
		this.dayId = dayId;
		this.dayName = dayName;
	}

	public DaySetupEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "DaySetupEntity [dayId=" + dayId + ", dayName=" + dayName + "]";
	}

	
	

}
