package com.zeroosoft.education.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="exam_summary")
public class ExamSummaryEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "summary_id")
    private Long examSummaryId;
	
	@Column(name = "batch_id")
	private Integer batchId;
	
	@Column(name = "exam_id")
	private Long examId;
	
	@Column(name = "exam_date")
	private Date examDate;
	
	@Column(name = "result_date")
    private Date resultDate;
	
	@Column(name = "entry_date_time")
	private Timestamp entryDateTime;

	@Column(name = "update_date_time")
	private Timestamp		lastUpdateDateTime;
	
	@Column(name = "entry_name")
	private String entryUserName;
	
	@Column(name = "update_username")
	private String lateUpdateUserName;

	public Long getSummaryId() {
		return examSummaryId;
	}

	public void setSummaryId(Long summaryId) {
		this.examSummaryId = summaryId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
	
	public Long getExamSummaryId() {
		return examSummaryId;
	}

	public void setExamSummaryId(Long examSummaryId) {
		this.examSummaryId = examSummaryId;
	}
	
	public Date getResultDate() {
		return resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}

	@Override
	public String toString() {
		return "ExamSummaryEntity [examSummaryId=" + examSummaryId + ", batchId=" + batchId + ", examId=" + examId
				+ ", examDate=" + examDate + ", resultDate=" + resultDate + ", entryDateTime=" + entryDateTime
				+ ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName=" + entryUserName
				+ ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}

	
}
