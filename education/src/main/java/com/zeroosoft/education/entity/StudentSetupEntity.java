package com.zeroosoft.education.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name="student_setup")
public class StudentSetupEntity 
{
	@Id
	@Column(name = "student_id")
    private String studentId;
	
	@Column(name = "student_name")
	private String studentName;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Column(name = "batch_id")
	private Integer batchId;
	
	@Column(name = "educational_institute")
	private String educationalInstitute;
	
	@Column(name = "contact_number")
	private String contactNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "father_name")
	private String fatherName;
	
	@Column(name = "mother_name")
	private String motherName;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "entry_date_time")
	private Timestamp entryDateTime;
	
	@Column(name = "entry_name")
	private String entryUserName;
	
	@Column(name = "update_date_time")
	private Timestamp		lastUpdateDateTime;
	
	@Column(name = "update_username")
	private String lastUpdateUserName;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getEducationalInstitute() {
		return educationalInstitute;
	}

	public void setEducationalInstitute(String educationalInstitute) {
		this.educationalInstitute = educationalInstitute;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getLastUpdateUserName() {
		return lastUpdateUserName;
	}

	public void setLastUpdateUserName(String lastUpdateUserName) {
		this.lastUpdateUserName = lastUpdateUserName;
	}

	@Override
	public String toString() {
		return "StudentSetupEntity [studentId=" + studentId + ", studentName=" + studentName + ", gender=" + gender
				+ ", dateOfBirth=" + dateOfBirth + ", batchId=" + batchId + ", educationalInstitute="
				+ educationalInstitute + ", contactNumber=" + contactNumber + ", email=" + email + ", fatherName="
				+ fatherName + ", motherName=" + motherName + ", address=" + address + ", entryDateTime="
				+ entryDateTime + ", entryUserName=" + entryUserName + ", lastUpdateDateTime=" + lastUpdateDateTime
				+ ", lastUpdateUserName=" + lastUpdateUserName + "]";
	}

	
}
