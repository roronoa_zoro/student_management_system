package com.zeroosoft.education.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.zeroosoft.education.entity.ExamIdentityEntity;

@Entity
@Table(name="exam_setup")
public class ExamSetupEntity 
{
	@EmbeddedId
	private ExamIdentityEntity examIdentity;
	
	@Column(name = "exam_name")
	private String examName;
	
	@Column(name = "syllabus")
	private String syllabus;
	
	@Column(name = "duration")
	private String duration;
	
	@Column(name = "total_marks")
	private Double totalMarks;
	
	@Column(name = "entry_date_time")
	private Timestamp entryDateTime;

	@Column(name = "update_date_time")
	private Timestamp		lastUpdateDateTime;
	
	@Column(name = "entry_name")
	private String entryUserName;
	
	@Column(name = "update_username")
	private String lateUpdateUserName;

	public ExamIdentityEntity getExamIdentity() {
		return examIdentity;
	}

	public void setExamIdentity(ExamIdentityEntity examIdentity) {
		this.examIdentity = examIdentity;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Double getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Double totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}

	@Override
	public String toString() {
		return "ExamSetupEntity [examIdentity=" + examIdentity + ", examName=" + examName + ", syllabus=" + syllabus
				+ ", duration=" + duration + ", totalMarks=" + totalMarks + ", entryDateTime=" + entryDateTime
				+ ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName=" + entryUserName
				+ ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}

	
}
