package com.zeroosoft.education.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "chapters")
public class ChapterEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "chapter_id", nullable = false, unique = true)
	private int chapterId;
	
	@Column(name="chapter_name", nullable = false)
	private String chapterName;
	
	public int getChapterId() {
		return chapterId;
	}
	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}
	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}
	@Override
	public String toString() {
		return "ChapterEntity [chapterId=" + chapterId + ", chapterName=" + chapterName + "]";
	}
	
	
}
