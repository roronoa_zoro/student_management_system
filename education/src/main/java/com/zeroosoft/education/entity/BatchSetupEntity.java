package com.zeroosoft.education.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.zeroosoft.education.dto.BatchSetup;

@Entity
@Table(name="batch_setup")
public class BatchSetupEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "batch_id")
	private Integer batchId;
	
	@Column(name = "batch_name")
	private String batchName;
	
	@Column(name = "session")
	private String session;
	
	@Column(name = "shift_start")
	private 	String  shiftStart;
	
	@Column(name = "shift_end")
	private 	String  shiftEnd;
	
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name="batch_day_setup",
            joinColumns = {
                    @JoinColumn(name = "batch_id", referencedColumnName = "batch_id")},
            inverseJoinColumns = {
                    @JoinColumn(name="day_id", referencedColumnName = "day_id")
            }
    )
    private Set<DaySetupEntity> days;
	
	@Column(name = "entry_date_time")
	private Timestamp entryDateTime;

	@Column(name = "update_date_time")
	private Timestamp		lastUpdateDateTime;
	
	@Column(name = "entry_name")
	private String entryUserName;
	
	@Column(name = "update_username")
	private String lateUpdateUserName;
	
	public BatchSetupEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public BatchSetupEntity(BatchSetup batch) 
	{
		this.batchId = batch.getBatchId();
		this.batchName = batch.getBatchName();
		this.session = batch.getSession();
		this.shiftStart = batch.getShiftStart();
		this.shiftEnd = batch.getShiftEnd();
		this.entryDateTime=batch.getEntryDateTime();
		this.entryUserName=batch.getEntryUserName();
	}



	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	
	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	public String getShiftStart() {
		return shiftStart;
	}

	public void setShiftStart(String shiftStart) {
		this.shiftStart = shiftStart;
	}

	public String getShiftEnd() {
		return shiftEnd;
	}

	public void setShiftEnd(String shiftEnd) {
		this.shiftEnd = shiftEnd;
	}
	
	public Set<DaySetupEntity> getDays() {
		return days;
	}

	public void setDays(Set<DaySetupEntity> days) {
		this.days = days;
	}
	
	public void addDay( DaySetupEntity day) {
        if(days == null) {
            days = new HashSet<>();
        }
        days.add(day);
    }


	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}
	
	@Override
	public String toString() {
		return "BatchSetupEntity [batchId=" + batchId + ", batchName=" + batchName + ", session=" + session
				+ ", shiftStart=" + shiftStart + ", shiftEnd=" + shiftEnd + ", entryDateTime=" + entryDateTime
				+ ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName=" + entryUserName
				+ ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}

	
}
