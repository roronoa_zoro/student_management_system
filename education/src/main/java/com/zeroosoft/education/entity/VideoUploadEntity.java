package com.zeroosoft.education.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="video_upload")
public class VideoUploadEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "video_id")
	private long	videoId;
	@Column(name = "level_id")
	private int		levelId;
	@Column(name = "chapter_id")
	private int 	chapterId;
	@Column(name = "video_name")
	private String 	videoName;
	@Column(name = "video_src")
	private String 	videoSrc;
	
	public long getVideoId() {
		return videoId;
	}

	public void setVideoId(long videoId) {
		this.videoId = videoId;
	}

	public int getLevelId() {
		return levelId;
	}
	
	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName.trim();
	}

	public String getVideoSrc() {
		return videoSrc;
	}

	public void setVideoSrc(String videoSrc) {
		this.videoSrc = videoSrc;
	}

	@Override
	public String toString() {
		return "VideoUploadEntity [videoId=" + videoId + ", levelId=" + levelId + ", chapterId=" + chapterId
				+ ", videoName=" + videoName + ", videoSrc=" + videoSrc + "]";
	}
	
	
}
