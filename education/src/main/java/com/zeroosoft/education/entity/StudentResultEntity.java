package com.zeroosoft.education.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student_result")
public class StudentResultEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "entry_id")
    private Long entryId;
    
	@Column(name = "summary_id")
    private Long examSummaryId;
    
	@Column(name = "student_id")
    private String studentId;
    
	@Column(name = "student_marks")
    private Double studentMarks;
    	
	@Column(name = "entry_name")
	private String entryUserName;
	
	@Column(name = "entry_date_time")
	private Timestamp entryDateTime;
	
	@Column(name = "update_date_time")
	private Timestamp		lastUpdateDateTime;
	
	@Column(name = "update_username")
	private String lateUpdateUserName;

	public Long getEntryId() {
		return entryId;
	}

	public void setEntryId(Long entryId) {
		this.entryId = entryId;
	}

	public Long getExamSummaryId() {
		return examSummaryId;
	}

	public void setExamSummaryId(Long examSummaryId) {
		this.examSummaryId = examSummaryId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public Double getStudentMarks() {
		return studentMarks;
	}

	public void setStudentMarks(Double studentMarks) {
		this.studentMarks = studentMarks;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}

	@Override
	public String toString() {
		return "StudentResultEntity [entryId=" + entryId + ", examSummaryId=" + examSummaryId + ", studentId="
				+ studentId + ", studentMarks=" + studentMarks + ", entryUserName=" + entryUserName + ", entryDateTime="
				+ entryDateTime + ", lastUpdateDateTime=" + lastUpdateDateTime + ", lateUpdateUserName="
				+ lateUpdateUserName + "]";
	}

	
}
