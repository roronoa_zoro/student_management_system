package com.zeroosoft.education.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Embeddable
public class ExamIdentityEntity implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "batch_id")
	private Integer batchId;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_id")
	private Long examId;

	public ExamIdentityEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamIdentityEntity(Integer batchId, Long examId) {
		super();
		this.batchId = batchId;
		this.examId = examId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	@Override
	public String toString() {
		return "ExamIdentityEntity [batchId=" + batchId + ", examId=" + examId + "]";
	}
	
	

}
