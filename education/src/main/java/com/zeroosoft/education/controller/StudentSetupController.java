package com.zeroosoft.education.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.zeroosoft.education.dto.StudentSetup;
import com.zeroosoft.education.service.BatchSetupService;
import com.zeroosoft.education.service.StudentSetupService;

@Controller
public class StudentSetupController 
{
	@Autowired BatchSetupService batchService;
	@Autowired StudentSetupService studentService;
	private static final Logger      logger = LoggerFactory.getLogger(StudentSetupController.class);
	
	
	@ResponseBody
	@PostMapping("check-duplicate-student-id")
	public boolean getStudentId(@RequestParam("studentId") String studentId)
	{
		return studentService.checkDuplicateStudentId(studentId);
	}
	
	
	@GetMapping("student-add.html")
	public String studentAdd(Model model)
	{
		model.addAttribute("student", new StudentSetup());
		model.addAttribute("batchList", batchService.showAllBatch());
		return "education/setup/student-setup/student-add";
	}
	
	@PostMapping("student-add-save.html")
	public String studentAddSave(Model model,@Valid @ModelAttribute StudentSetup student,BindingResult result,RedirectAttributes redirectAttribute)
	{
		if(result.hasErrors())
		{
			logger.info("Error: "+ result.getAllErrors());
			redirectAttribute.addFlashAttribute("errorMessage","Please Check All Input Fields");
		}
		else
		{
			redirectAttribute.addFlashAttribute("successMessage","Student Name: "+student.getStudentName()+" is added Successfully");
			studentService.addStudentInfo(student);
		}
		return "redirect:student-add.html";
	}
	
	@GetMapping("student-list.html")
    public String studentList(Model model)
    { 
       List<StudentSetup> studentList =  studentService.getStudentList();
       for(StudentSetup studentInfo: studentList)
       {
         String batchName = batchService.showBatchInfo(studentInfo.getBatchId()).getBatchName();
         studentInfo.setBatchName(batchName);
      }
       
      model.addAttribute("studentList", studentList);
      return "education/setup/student-setup/student-list";
    }
	
	@GetMapping("student-edit.html")
	public String studentEdit(Model model, @RequestParam("studentId") String studentId)
	{
		String strDate;
		StudentSetup studentSetup = studentService.showStudentInfo(studentId);
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
		if(studentSetup.getDateOfBirth()!=null)
	       strDate = formatter.format(studentSetup.getDateOfBirth());
		else
			strDate="";
	    studentSetup.setDOB(strDate);
	    model.addAttribute("student",studentSetup);

	    model.addAttribute("batchList",batchService.showAllBatch());

	    return "education/setup/student-setup/student-edit";
	}

	@PostMapping("student-edit-save.html")
	public String studentEditSave(Model model,@ModelAttribute @Valid StudentSetup student,BindingResult result) throws ParseException
	{
	    if(result.hasErrors())
	    {
	        return "redirect:student-list.html";
	    }
	    else
	    {
	        studentService.updateStudentInfo(student);
	        return "redirect:student-list.html";
	    }
	}

	@GetMapping("student-delete.html")
	public String studentDelete(Model model,@RequestParam("studentId") String studentId)
	{
	  studentService.deleteStudentInfo(studentId);
      return "redirect:student-list.html";
	}
}


