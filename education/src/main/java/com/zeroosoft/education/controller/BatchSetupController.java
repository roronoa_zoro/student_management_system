package com.zeroosoft.education.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.zeroosoft.education.dto.BatchSetup;
import com.zeroosoft.education.dto.DaySetup;
import com.zeroosoft.education.service.BatchSetupService;
import com.zeroosoft.education.service.DaySetupService;

@Controller
public class BatchSetupController {

	@Autowired BatchSetupService batchService;
	@Autowired DaySetupService dayService;
	
	
	@GetMapping("batch-add.html")
	public String batchSetup(Model model)
	{
		BatchSetup batch = new BatchSetup();
		batch.setDays(dayService.dayList());
		model.addAttribute("batch",batch);
		model.addAttribute("batchId",batchService.getAutoIncrementId());
		return "education/setup/batch-setup/batch-add";
	}
	
	@ResponseBody
	@PostMapping("getbatchName")
	public String getTypeName(@RequestParam("batchName") String batchName) {
		
		String str_result = batchService.getMatchingPattern(batchName);
		return str_result;
		
	}
	
	
	@PostMapping("batch-add-save.html")
	public String batchSetupSave(Model model,@ModelAttribute @Valid BatchSetup batch,BindingResult result,RedirectAttributes redirectAttribute)
	{
		if(result.hasErrors())
		{
			redirectAttribute.addFlashAttribute("errorMessage","Invalid Input");
		}
		else
		{
			redirectAttribute.addFlashAttribute("successMessage","Batch Name: "+batch.getBatchName()+" is added Successfully");
			List<DaySetup> daySetupList = new ArrayList<DaySetup>(); 
			
			for(DaySetup day: batch.getDays())
			{
				if(day.isSelected())
					daySetupList.add(day);
			}
			batch.getDays().clear();
			
			batch.setDays(daySetupList);
			
			batchService.addBatch(batch);
		}
		return "redirect:batch-add.html";
	}
	
	@GetMapping("batch-list.html")
	public String batchList(Model model)
	{
		model.addAttribute("batchList", batchService.showAllBatch());
		
		return "education/setup/batch-setup/batch-list";
	}
	
	@GetMapping("batch-edit.html")
	public String batchEdit(Model model,@RequestParam("batchId") Integer batchId)
	{
	     BatchSetup batch = batchService.showBatchInfo(batchId);
	     model.addAttribute("batch",batch);
	     return "education/setup/batch-setup/batch-edit";
	}
	
	@PostMapping("batch-edit-save.html")
	public String batchEditSave(Model model,@ModelAttribute @Valid BatchSetup batch,BindingResult result)
	{
		batchService.updateBatch(batch);
		return "redirect:batch-list.html";
	}
	
	@GetMapping("batch-delete.html")
	public String batchDelete(Model model,@ModelAttribute @Valid BatchSetup batch,BindingResult result)
	{
		batchService.deleteBatch(batch);
		return "redirect:batch-list.html";
	}
}
