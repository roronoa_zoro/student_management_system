package com.zeroosoft.education.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zeroosoft.education.dto.Chapter;
import com.zeroosoft.education.dto.VideoUpload;
import com.zeroosoft.education.service.ChapterService;
import com.zeroosoft.education.service.VideoUploadService;

@Controller
public class YoutubeVideoController {
	@Autowired private VideoUploadService videoService;
	@Autowired private ChapterService		chapterService;
	
	Logger logger = LoggerFactory.getLogger(YoutubeVideoController.class);
	
	@ModelAttribute("chapters")
	public List<Chapter> getAllChapters(){
		List<Chapter> chapters = chapterService.getAllChapters();
		List<VideoUpload> videoUploads = videoService.getAllVideos();
		for (Chapter chapter : chapters) {
			List<VideoUpload> list = new ArrayList<VideoUpload>();
			for (VideoUpload videoUpload : videoUploads) {
				if (videoUpload.getChapterId() == chapter.getChapterId()) {
					list.add(videoUpload);
				}
			}
			chapter.setVideoUploads(list);
		}
		
		return chapters;
	}
	
	@GetMapping("video.html")
	public String videoUploadShow(Model model, @RequestParam(defaultValue = "0") Long videoId,@RequestParam(defaultValue = "0") int chapterId) {
		
		VideoUpload video;
		Chapter chapter;
		List<VideoUpload> videos = videoService.getAllVideos();
		List<Chapter> chapters = chapterService.getAllChapters();
		if (videoId == 0) {
			chapter = chapters.get(0);
			video = videos.get(0);
			
		}else {
			video = videoService.getVideo(videoId);
			chapter = chapterService.getChapter(chapterId);
		}
		
		String videoSrc = video.getVideoSrc();
		int i = videoSrc.indexOf("=");
		String subStr = videoSrc.substring(i+1, videoSrc.length());
		String src = "https://www.youtube.com/embed/" + subStr;
		model.addAttribute("src", src);
		System.out.println("activeId" + video.getVideoId());
		model.addAttribute("activeVideoId", video.getVideoId());
		model.addAttribute("activeChapterId", chapter.getChapterId());
		
		
		return "education/video/video";
	}
	
	
	
	/*
	 * @GetMapping("videoUpload")
	 * 
	 * @ResponseBody public String streamVideo(Model model, @RequestParam Long
	 * videoId) { System.out.println("value: " + videoId); String src = null;
	 * VideoUpload video = videoService.getVideo(videoId);
	 * 
	 * String videoSrc = video.getVideoSrc(); int i = videoSrc.indexOf("="); String
	 * subStr = videoSrc.substring(i+1, videoSrc.length()); src =
	 * "https://www.youtube.com/embed/" + subStr; System.out.println("src" + src);
	 * 
	 * return src; }
	 */
	
	@GetMapping("videoUpload") 
	public String streamVideo(Model model, @RequestParam Long videoId) {
		System.out.println("value: " + videoId);
		String src = null;
		List<VideoUpload> videos = videoService.getAllVideos();
		VideoUpload video = videoService.getVideo(videoId);
		
		String videoSrc = video.getVideoSrc();
		int i = videoSrc.indexOf("=");
		String subStr = videoSrc.substring(i+1, videoSrc.length());
		src = "https://www.youtube.com/embed/" + subStr;
		System.out.println("src" + src);
		model.addAttribute("src", src);
		model.addAttribute("videos", videos);
	
		return "education/video/video";
	}
	 
	
}
