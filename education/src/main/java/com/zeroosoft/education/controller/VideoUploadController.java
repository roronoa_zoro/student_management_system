package com.zeroosoft.education.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zeroosoft.education.dto.Chapter;
import com.zeroosoft.education.dto.StudentLevel;
import com.zeroosoft.education.dto.VideoUpload;
import com.zeroosoft.education.service.ChapterService;
import com.zeroosoft.education.service.VideoUploadService;

@Controller
public class VideoUploadController {
	@Autowired private VideoUploadService videoService;
	@Autowired private ChapterService     chapterService;
	
	Logger logger = LoggerFactory.getLogger(VideoUploadController.class);
	
	@ModelAttribute("levelList")
	public List<StudentLevel> getAllLevels(){
		List<StudentLevel> list = new ArrayList<StudentLevel>();
		list.add(new StudentLevel(1,"Math 1st Paper"));
		list.add(new StudentLevel(2,"Math 2nd Paper"));
		return list;
				
	}
	
	@ModelAttribute("chapters")
	public List<Chapter> getAllChapters(){
		return chapterService.getAllChapters();
	}
	
	@GetMapping("video-upload.html")
	public String videoUpload(Model model) {
		VideoUpload video = new VideoUpload();
		model.addAttribute("video", video);
		return "education/video/video-upload";
		
		
	}
	
	
	@PostMapping("video-upload-save")
	@ResponseBody
	public String videoUploadSave(@ModelAttribute @Valid VideoUpload video, BindingResult result, Model model) {
		String response = "";
		logger.debug("video: " + video);
		if (result.hasErrors()) {
			response = "please check your input";
			return response;
		}
		VideoUpload videoUpload = videoService.addVideo(video);
		logger.info("videoUpload: " + videoUpload);
		response = "Video info successfully upload";
		
		return response;
	}
	
	@GetMapping("video-upload-list.html")
	public String videoUploadList(Model model) {
		List<VideoUpload> videos = videoService.getAllVideos();
		model.addAttribute("videos", videos);
		return "education/video/video-upload-list";
	}
	
	@GetMapping("updateVideo")
	@ResponseBody
	public VideoUpload updateVideo(Model model, @RequestParam long videoId) {
		VideoUpload video = videoService.getVideo(videoId);
		model.addAttribute("video", video);
		return video;
	}
	
	@PostMapping("video-upload-update-save")
	@ResponseBody
	public String videoUploadUpdateSave(@ModelAttribute @Valid VideoUpload video, BindingResult result, Model model) {
		String response = "";
		logger.debug("video: " + video);
		
		if (result.hasErrors()) 
		{ 
			response = "please check your input"; 
			return  response; 
		} 
		VideoUpload videoUpload = null;
		try {
			videoUpload = videoService.UpdateVideo(video);
			response = "Video info successfully upload";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("videoUpload: " + videoUpload);
		 
		
		
		return response;
	}
	
	@GetMapping("/deleteVideo")
	public String deleteVideo(@RequestParam long videoId, RedirectAttributes redirect) {
		videoService.deleteVideo(videoId);
		redirect.addFlashAttribute("successMessage", "Video successfully deleted!");
		return "redirect:/video-upload-list.html";
	}
	
	
	@GetMapping("checkVideoNameDuplication")
	@ResponseBody
	public String checkVideoNameDuplication(HttpServletResponse response,@RequestParam String videoName) {
		logger.info("videoName: " + videoName);
		Boolean isAvailable = videoService.checkVideoNameDuplication(videoName.trim());
		return isAvailable.toString();
	}
	
	
	
}
