package com.zeroosoft.education.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TutorialController {
	
	@GetMapping("demo.html")
	public String demoCode() {
		return "tutorial/demo";
	}
	
	@GetMapping("/general.html")
	public String generalForm() {
		return "tutorial/general-form";
	}
	
	@GetMapping("/advanced.html")
	public String advancedForm() {
		return "tutorial/advanced-form";
	}
	
	@GetMapping("/data-table.html")
	public String dataTable() {
		return "tutorial/data-table";
	}
	@GetMapping("/validation-form.html")
	public String validationForm() {
		return "tutorial/validation-form";
	}
}
