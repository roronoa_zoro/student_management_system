package com.zeroosoft.education.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.DocumentException;
import com.zeroosoft.education.dto.ExamSetup;
import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.dto.StudentResult;
import com.zeroosoft.education.dto.StudentResultList;
import com.zeroosoft.education.service.BatchSetupService;
import com.zeroosoft.education.service.ExamSetupService;
import com.zeroosoft.education.service.ExamSummaryService;
import com.zeroosoft.education.service.StudentResultService;
import com.zeroosoft.education.service.StudentSetupService;

@Controller
public class StudentResultController 
{
	@Autowired BatchSetupService batchService;
	@Autowired ExamSummaryService examSummaryService;
	@Autowired ExamSetupService examSetupService;
	@Autowired StudentResultService resultService;
	@Autowired StudentSetupService studentSetupService;
	
	private static final Logger     logger = LoggerFactory.getLogger(StudentResultController.class);
	
	@GetMapping("batch-wise-student-result-entry-search.html")
    public String studentResultSearch(Model model)
    {
		model.addAttribute("batchList",batchService.showAllBatch());
		return "education/transaction/student-result/batch-wise-student-result-entry-search";
    }
	
	@ResponseBody
	@PostMapping("get-exam-Id")
	public List<ExamSetup> examIdList(@RequestParam("batchName") Integer batchId)
	{
		List<ExamSetup> examList = new ArrayList<ExamSetup>();
		List<ExamSetup> storedExamList = examSetupService.showExamListSearchById(batchId);
		for(ExamSetup exam: storedExamList)
		{
			Long examId = exam.getExamId();
			ExamSummary examSummary = examSummaryService.getExamSummaryInfo(batchId, examId);
			if(examSummary==null)
			{
				examList.add(exam);
				logger.info("Exam : "+exam);
			}
				
		}
		if(examList.size()>0)
		Collections.reverse(examList);
		return examList;
	}
	
	@PostMapping("/batch-wise-student-result-entry.html")
	public String resultShow(Model model,HttpServletRequest request)
	{
		Integer batchId = Integer.parseInt(request.getParameter("batchName"));
		Long examId = Long.parseLong(request.getParameter("examName"));
		List<StudentResult> results = resultService.showRegisteredStudentForResult(batchId);
		StudentResultList studentResultList = new StudentResultList();
		studentResultList.setResults(results);
		
		model.addAttribute("resultList", studentResultList);
		model.addAttribute("batch", batchService.showBatchInfo(batchId));
		model.addAttribute("exam",examSetupService.showExamInfo(examId, batchId));
		return "education/transaction/student-result/batch-wise-student-result-entry";
	}
	
	@PostMapping("student-result-save.html")
	public String studentResultSave(Model model,@Valid @ModelAttribute StudentResultList resultList,HttpServletRequest request) throws ParseException
	{
		Integer batchId = Integer.parseInt(request.getParameter("batchId"));
		Long examId = Long.parseLong(request.getParameter("examId"));
		String str_examDate = request.getParameter("examDate");
		String str_resultDate = request.getParameter("resultDate");
		Date examDate=new SimpleDateFormat("dd/MM/yyyy").parse(str_examDate);
		Date resultDate=new SimpleDateFormat("yyyy/MM/dd").parse(str_resultDate);
		
		//setup & save ExamSummary object properties
		
		ExamSummary examSummary = new ExamSummary(examId,batchId,examDate,resultDate);
		Long examSummaryId = examSummaryService.addExamSummary(examSummary);
		
		// save data in StudentResultEntity table
		
		for(StudentResult result: resultList.getResults())
		{
			result.setExamSummaryId(examSummaryId);
			resultService.studentResultSave(result);
		}
		
		return "education/transaction/student-result/batch-wise-student-result-entry-search";
	}
	
	@GetMapping("batch-wise-student-result-list-search.html")
	public String studentResultListSearch(Model model)
    {
		model.addAttribute("batchList",batchService.showAllBatch());
		return "education/transaction/student-result/batch-wise-student-result-list-search";
    }
	
	@ResponseBody
	@PostMapping("get-exam-Id-for-result-show")
	public List<ExamSetup> examIdForResult(@RequestParam("batchName") Integer batchId)
	{
		List<ExamSetup> examList = new ArrayList<ExamSetup>();
		List<ExamSetup> storedExamList = examSetupService.showExamListSearchById(batchId);
		for(ExamSetup exam: storedExamList)
		{
			Long examId = exam.getExamId();
			ExamSummary examSummary = examSummaryService.getExamSummaryInfo(batchId, examId);
			if(examSummary!=null)
			{
				logger.info("Exam List: "+exam);
				examList.add(exam);
			}
				
		}
		if(examList.size()>0)
		Collections.reverse(examList);
		return examList;
	}
	
	
	@PostMapping("/batch-wise-student-result-list-show.html")
	public String batchWiseResultShow(Model model,HttpServletRequest request)
	{
		Integer batchId = Integer.parseInt(request.getParameter("batchName"));
		Long examId = Long.parseLong(request.getParameter("examName"));
		ExamSummary examSummary = examSummaryService.getExamSummaryInfo(batchId, examId);
		List<StudentResult> results  = resultService.showResult(examSummary);
		model.addAttribute("results", results);
		model.addAttribute("exam",examSetupService.showExamInfo(examId, batchId));
		model.addAttribute("batch", batchService.showBatchInfo(batchId));
		model.addAttribute("examSummary",examSummary);
		return "education/transaction/student-result/batch-wise-student-result-list-show";
	}
	
	@GetMapping("/batch-wise-student-result-update.html")
	public String studentResultUpdate(Model model,@RequestParam("entryId") Long entryId)
	{
		StudentResult studentResult = resultService.getStudentResultInfo(entryId);
		String studentName = studentSetupService.showStudentInfo(studentResult.getStudentId()).getStudentName();
		studentResult.setStudentName(studentName);
		ExamSummary examSummary = examSummaryService.getExamSummaryBySummaryId(studentResult.getExamSummaryId());
		model.addAttribute("studentResult", studentResult);
		model.addAttribute("examSummary",examSummary);
		return "education/transaction/student-result/batch-wise-student-result-update";
	}
	
	@PostMapping("/batch-wise-student-result-update-save.html")
	public String studentResultUpdateSave(Model model,StudentResult studentResult,HttpServletRequest request) throws Exception
	{
		StudentResult storedResult  = resultService.getStudentResultInfo(studentResult.getEntryId());
		
		//update studentResult table
		
		double value1 = storedResult.getStudentMarks();
		double value2 = studentResult.getStudentMarks();
		if(value1!=value2)
		{
			storedResult.setStudentMarks(studentResult.getStudentMarks());
			resultService.studentResultUpdate(storedResult);
		}
		
		//update examSummary table when result date and/or exam date is updated
		
		ExamSummary storedExamSummary = examSummaryService.getExamSummaryBySummaryId(storedResult.getExamSummaryId());
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		String str_examDate = request.getParameter("examDate").substring(6)+"-"+request.getParameter("examDate").substring(3,5)+"-"+request.getParameter("examDate").substring(0, 2);
		String str_resultDate = request.getParameter("resultDate").substring(6)+"-"+request.getParameter("resultDate").substring(3,5)+"-"+request.getParameter("resultDate").substring(0, 2);	
	    logger.info("Stored Exam Info: "+ storedExamSummary);
		Date d1 = storedExamSummary.getExamDate();
	    Date d2 = sdformat.parse(str_examDate);
	    int examDateCompare = d1.compareTo(d2);
	    
	    Date d3 = storedExamSummary.getResultDate();
	    Date d4 = sdformat.parse(str_resultDate);
	    int resultDateCompare = d3.compareTo(d4);
	    
	    if(examDateCompare!=0 || resultDateCompare!=0)
	    {
	    	if(examDateCompare!=0)
	    	{
	    		storedExamSummary.setExamDate(d2);
	    	}
	    	if(resultDateCompare!=0)
	    	{
	    		storedExamSummary.setResultDate(d4);
	    	}
	 	    examSummaryService.updateExamSummary(storedExamSummary);
	    }
	    
	    return "redirect:batch-wise-student-result-list-search.html";
	}
	
	@ResponseBody
	@GetMapping("get-last-result-info.html")
	public void lastResultInfo(HttpServletResponse response) throws IOException, DocumentException
	{
		OutputStream stream = response.getOutputStream();
	    ExamSummary examSummary = resultService.getLastStudentResult();
	    resultService.showLastResult(examSummary,stream);
	}
	
}
