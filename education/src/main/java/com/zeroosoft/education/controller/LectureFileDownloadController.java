package com.zeroosoft.education.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.zeroosoft.education.dto.LectureFile;
import com.zeroosoft.education.service.LectureFileUploadService;

@Controller
public class LectureFileDownloadController {
	@Autowired 	ServletContext context;
	@Autowired	private LectureFileUploadService fileUploadService;;

	@GetMapping("downloadFile")
	public void downloadFile(@RequestParam("fileName") String fileName, @RequestParam("modifiedFileName") String modifiedFileName,
			HttpServletResponse response) {

		String fullPath = context.getRealPath("/images/" + File.separator + modifiedFileName);

		File file = new File(fullPath);
		final int BUFFER_SIZE = 4096;

		if (file.exists()) {
			try {
				FileInputStream inputStream = new FileInputStream(file);
				String mimeType = context.getMimeType(fullPath);
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
				OutputStream outputStream = response.getOutputStream();
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				inputStream.close();
				outputStream.close();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}
	
	
	@GetMapping("/downloadZipFile")
	public void downloadZipFile (HttpServletResponse response){
		System.out.println("i m in");
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			byte bytes[] = new byte[122048];
			List<LectureFile> files = fileUploadService.getAllLectureFiles();
			for (LectureFile file : files) {
				if (file.getModifiedFileName() != null && StringUtils.hasText(file.getModifiedFileName())) {
					FileInputStream fis = new FileInputStream(context.getRealPath("/images/" + File.separator + file.getModifiedFileName()));
					BufferedInputStream bis = new BufferedInputStream(fis);
					zos.putNextEntry(new ZipEntry(file.getFileName()));
					int bytesRead;
					while ((bytesRead = bis.read(bytes)) != -1) {
						zos.write(bytes, 0 , bytesRead);
					}
					zos.closeEntry();
					bis.close();
					fis.close();
				}
			}
			zos.flush();
			baos.flush();
			zos.close();
			baos.close();
			
			byte[] zip = baos.toByteArray();
			ServletOutputStream sos = response.getOutputStream();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=files.zip");
			sos.write(zip);
			sos.flush();
			sos.close();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
