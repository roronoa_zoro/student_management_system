package com.zeroosoft.education.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zeroosoft.education.dto.BatchSetup;
import com.zeroosoft.education.dto.ExamSetup;
import com.zeroosoft.education.service.BatchSetupService;
import com.zeroosoft.education.service.ExamSetupService;

@Controller
public class ExamSetupController 
{
	@Autowired ExamSetupService examService;
	@Autowired BatchSetupService batchService;
	
	@GetMapping("exam-add.html")
	public String examAdd(Model model)
	{
		model.addAttribute("batchList", batchService.showAllBatch());
		model.addAttribute("exam",new ExamSetup());
		return "education/setup/exam-setup/exam-add";
	}

	@PostMapping("exam-add-save.html")
	public String examAddSave(Model model,@Valid @ModelAttribute ExamSetup examSetup,BindingResult result,RedirectAttributes redirectAttribute)
	{
		if(result.hasErrors())
		{
			redirectAttribute.addFlashAttribute("errorMessage","Please Check All Input Fields");
		}
		else
		{
			redirectAttribute.addFlashAttribute("successMessage","Exam Name: "+examSetup.getExamName()+" is added Successfully");
			examService.examSave(examSetup);
		}
		return "redirect:exam-add.html";
	}
	
	@GetMapping("batch-wise-exam-list-search.html")
	public String batchWiseExamListSearch(Model model)
	{
		model.addAttribute("batch", new BatchSetup());
		model.addAttribute("batchList", batchService.showAllBatch());
		return "education/setup/exam-setup/batch-wise-exam-list-search";
	}
	
	@GetMapping("exam-list.html")
	public String examList(Model model,@RequestParam("batchName") Integer batchId)
	{
		model.addAttribute("batch", new BatchSetup());
		model.addAttribute("batchList", batchService.showBatchInfo(batchId));
		model.addAttribute("examList",examService.showExamListSearchById(batchId));
		return "education/setup/exam-setup/batch-wise-exam-list-search";
	}
		
	@GetMapping("exam-edit.html")
	public String examEdit(Model model,@RequestParam("batchId") Integer batchId ,@RequestParam("examId") Long examId)
	{
		
		ExamSetup exam = examService.showExamInfo(examId, batchId);
		exam.setBatchId(batchId);
		model.addAttribute("exam",exam);
		model.addAttribute("batchName", batchService.showBatchInfo(batchId).getBatchName());
		return "education/setup/exam-setup/exam-edit";
	}
	
	@PostMapping("exam-edit-save.html")
	public String examEditSave(Model model,ExamSetup examSetup)
	{
		examService.examUpdate(examSetup);
		return "redirect:batch-wise-exam-list-search.html";
	}
	
	@GetMapping("exam-delete.html")
	public String examDelete(Model model,@RequestParam("batchId") Integer batchId ,@RequestParam("examId") Long examId)
	{
		ExamSetup exam = examService.showExamInfo(examId, batchId);
		examService.examDelete(exam);
		return "redirect:batch-wise-exam-list-search.html";
	}
	
	
}
