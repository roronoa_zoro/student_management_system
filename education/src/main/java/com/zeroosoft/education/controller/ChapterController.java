package com.zeroosoft.education.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zeroosoft.education.dto.Chapter;
import com.zeroosoft.education.service.ChapterService;

@Controller
public class ChapterController {
	
	@Autowired
	private ChapterService service;
	
	@GetMapping("chapter-add.html")
	public String chapterAdd(Model model) {
		Chapter chapter = new Chapter();
		
		model.addAttribute("chapter", chapter);
		
		return "education/chapter/chapter-add";
	}
	
	@PostMapping("chapter-add-save")
	public String chapterAddSave(@ModelAttribute @Valid Chapter chapter,Model model, BindingResult result, RedirectAttributes redirect) {
		//System.out.println("chapter" + chapter);
		if (result.hasErrors()) {
			model.addAttribute("chapter", chapter);
			model.addAttribute("errorMessage", "please check your input!");
			return "education/chapter/chapter-add";
		}
		if (service.duplicationCheck(chapter.getChapterName())) {
			model.addAttribute("chapter", new Chapter());
			model.addAttribute("errorMessage", chapter.getChapterName());
			return "education/chapter/chapter-add";
		}
		Chapter chap = service.addChapter(chapter);
		
		redirect.addFlashAttribute("successMessage", chap.getChapterName());
		
		return "redirect:/chapter-add.html";
	}
	
	@GetMapping("chapter-list.html")
	public String chapterList(Model model) {
		List<Chapter> chapters = service.getAllChapters();
		model.addAttribute("chapters", chapters );
		return "education/chapter/chapter-list";
	}
	
	@GetMapping("delete-chapter")
	public String deleteChapter(@RequestParam int chapterId, RedirectAttributes redirect) {
		service.deleteChapter(chapterId);
		redirect.addFlashAttribute("successMessage", "successfully deleted");
		
		return "redirect:/chapter-list.html";
	}
	
	@GetMapping("update-chapter")
	@ResponseBody
	public Chapter updateChapter(Model model, @RequestParam int chapterId) {
		Chapter chapter = service.getChapter(chapterId);
		return chapter;
	}
	
	@PostMapping("update-chapter-save")
	public String updateChapterSave(@ModelAttribute @Valid Chapter chapter,Model model, BindingResult result, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			model.addAttribute("chapter", chapter);
			model.addAttribute("errorMessage", "please check your input!");
			return "education/chapter/chapter-add";
		}
		if (service.duplicationCheck(chapter.getChapterName())) {
			model.addAttribute("chapter", new Chapter());
			model.addAttribute("errorMessage", chapter.getChapterName());
			return "education/chapter/chapter-add";
		}
		Chapter chap = service.updateChapter(chapter);
		
		redirect.addFlashAttribute("successMessage", chap.getChapterName());
		
		return "redirect:/chapter-list.html";
	}
	
}
