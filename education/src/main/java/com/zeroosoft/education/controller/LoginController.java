package com.zeroosoft.education.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
	@GetMapping("/")
	public String dashboard(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String user = auth.getName();
			
		System.out.println("user ===>: " + auth.getCredentials());
		System.out.println("role ===>: " + auth.getAuthorities());
		model.addAttribute("name", user);
		return "tutorial/demo";
	}

	@GetMapping("/login.html")
	public String login() {
		return "/login";
	}

}
