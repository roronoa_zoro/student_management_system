package com.zeroosoft.education.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zeroosoft.education.dto.LectureFile;
import com.zeroosoft.education.dto.StudentLevel;
import com.zeroosoft.education.service.LectureFileUploadService;

@Controller
public class LectureFileController {
	
	private static Logger logger = LoggerFactory.getLogger(LectureFileController.class);
	
	// file size limit = 2mb
	private final int limit_fileSize = 1024 * 1024* 2;
	
	@Autowired
	private LectureFileUploadService fileUploadService;
	
	@ModelAttribute("levelList")
	public List<StudentLevel> getAllLevels(){
		List<StudentLevel> list = new ArrayList<StudentLevel>();
		list.add(new StudentLevel(1,"Eleven"));
		list.add(new StudentLevel(2,"Twelve"));
		return list;
				
	}
	
	// lecture file show , download
	@GetMapping("/lecture-file-download.html")
	public String lectureFileDownload(Model model, @RequestParam int levelId) {
		model.addAttribute("lectures", fileUploadService.getAllLectureFiles());
		return "education/file/lecture-file-download";
	}
	
	// lecture file update, delete
	@GetMapping("/lecture-file-list.html")
	public String lectureFileList(Model model) {
		model.addAttribute("lectures", fileUploadService.getAllLectureFiles());
		model.addAttribute("isEdit", true);
		return "education/file/lecture-file-list";
	}
	
	// lecture file upload
	@GetMapping("/lecture-file-upload.html")
	public String lectureFileUpload(Model model) {
		model.addAttribute("lectureFile", new LectureFile());
		model.addAttribute("isEdit", false);
		return "education/file/lecture-file-upload";
	}
	
	@PostMapping("/lecture-file-upload-save")
	public String uploadFile(@ModelAttribute LectureFile lectureFile, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			Model model) {
		logger.info("file size" + FileUtils.byteCountToDisplaySize(file.getSize()));
		logger.info("lectureFile" + lectureFile);
		

		if (!file.isEmpty() && file.getSize() < limit_fileSize) {
			LectureFile lectureFile2 = fileUploadService.saveUploadFile(lectureFile , file);
			logger.info("lecture: " + lectureFile2);
			redirectAttributes.addFlashAttribute("successMessage",	"You successfully uploaded " + file.getOriginalFilename() + "!");
			return "redirect:/lecture-file-upload.html";
		} else {
			model.addAttribute("isEdit", true);
			model.addAttribute("errorMessage", "please try again!");
			return "/lecture-file-upload.html";
		}

	}
	
	@GetMapping("updateFile")
	public String editUploadFiles(@RequestParam long id, Model model) {
		System.out.println("id: " + id);
		
		LectureFile lectureFile = fileUploadService.getLectureFile(id);
		model.addAttribute("isEdit", true);
		//model.addAttribute("lectureFile", lectureFile);
		//model.addAttribute("levelId", file.getLevelId());
		model.addAttribute("lectureName", lectureFile.getLectureName());
		model.addAttribute("modifiedFileName", lectureFile.getModifiedFileName());
		model.addAttribute("fileExtension", lectureFile.getFileExtension());
		model.addAttribute("id", lectureFile.getFileId());

		return "education/file/lecture-file-upload";
	}
	
	@PostMapping("updateFileSave")
	public String editUploadFilesSave(@ModelAttribute LectureFile updateLectureFile, @RequestParam(value = "file", required=false) MultipartFile file, 
			@RequestParam(required=false) String removeFile, @RequestParam long id,
			RedirectAttributes redirectAttributes, Model model) {
		LectureFile lectureFile = fileUploadService.getLectureFile(id);
		lectureFile.setLectureName(updateLectureFile.getLectureName());
		lectureFile.setLevelId(updateLectureFile.getLevelId());
		lectureFile.setRemoveFile(updateLectureFile.getRemoveFile());
		logger.info("files: " + lectureFile);
		fileUploadService.updateLectureFile(lectureFile, file);
		
		redirectAttributes.addFlashAttribute("successMessage",
				"You successfully uploaded " + file.getOriginalFilename() + "!");
		return "redirect:/lecture-file-list.html";

	}
	
	@GetMapping("deleteLectureFile")
	public String deleteUploadFile(@RequestParam long id, Model model) {
		logger.info("i m in " + id);
		fileUploadService.deleteLectureFile(id);
		return "redirect:/lecture-file-list.html";
	}
	
	
	
	
}
