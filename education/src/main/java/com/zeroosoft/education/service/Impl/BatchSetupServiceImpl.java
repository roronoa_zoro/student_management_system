package com.zeroosoft.education.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zeroosoft.education.dto.BatchSetup;
import com.zeroosoft.education.dto.DaySetup;
import com.zeroosoft.education.entity.BatchSetupEntity;
import com.zeroosoft.education.entity.DaySetupEntity;
import com.zeroosoft.education.repo.BatchSetupRepository;
import com.zeroosoft.education.repo.DaySetupRepository;
import com.zeroosoft.education.service.BatchSetupService;
import com.zeroosoft.education.service.DaySetupService;

@Service
public class BatchSetupServiceImpl implements BatchSetupService
{
	
	private static final Logger      logger = LoggerFactory.getLogger(BatchSetupServiceImpl.class);
	
	@Autowired BatchSetupRepository batchRepository;
	@Autowired DaySetupRepository dayRepository;
	@Autowired DaySetupService dayService;

	@Override
	@Transactional
	public void addBatch(BatchSetup batch) 
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Timestamp currentDateTime = new Timestamp(System.currentTimeMillis());
		batch.setEntryDateTime(currentDateTime);
		BatchSetupEntity entity = new BatchSetupEntity(batch);	
		entity.setEntryUserName(authentication.getName());
		for(DaySetup day: batch.getDays())
		{
			DaySetupEntity target = new DaySetupEntity();
			BeanUtils.copyProperties(day, target);
			if(day.isSelected())
			{
				entity.addDay(target);
			}
		}
		
		logger.info("Batch Details: "+ entity);
		batchRepository.save(entity);
	}

	@Override
	public BatchSetup showBatchInfo(Integer id) 
	{
		BatchSetupEntity entity = batchRepository.findById(id).get();
		if(entity==null)
			return null;
		else
		{
			List<DaySetup> days = new ArrayList<DaySetup>();
			Map<Integer,DaySetup> daysMap  = new HashMap<Integer,DaySetup>();
			BatchSetup dto = new BatchSetup();
			for(DaySetup day: dayService.dayList())
			{
				daysMap.put(day.getDayId(), day);
			}
			for(DaySetupEntity dayEntity: entity.getDays())
			{
				DaySetup day = new DaySetup(dayEntity);
				day.setSelected(true);
				daysMap.put(day.getDayId(), day);
			}
			
			for(int dayNumber=1;dayNumber<=7;dayNumber++)
			{
				days.add(daysMap.get(dayNumber));
			}
			
			BeanUtils.copyProperties(entity, dto);
			dto.setDays(days);
			logger.info("Batch Info: "+dto);
			return dto;
		}
	}

	@Override
	public List<BatchSetup> showAllBatch() {
		
		List<BatchSetupEntity> showStoredDate = batchRepository.findAll();
		List<BatchSetup> batchList = new ArrayList<BatchSetup>();
		if(CollectionUtils.isNotEmpty(showStoredDate))
		{
			for(BatchSetupEntity entity: showStoredDate)
			{
				BatchSetup dto = new BatchSetup();
				BeanUtils.copyProperties(entity, dto);
				String str="";
				
				for(DaySetupEntity day: entity.getDays())
				{
					if(str.length()==0)
				    	str = str+day.getDayName();
					else
						str = str+" , "+day.getDayName();
				}
				dto.setDayNames(str);
				batchList.add(dto);
			}
			return batchList;
		}
		else
		 return Collections.emptyList();
	}

	@Override
	public Integer getAutoIncrementId() {
		List<BatchSetup> batchList = showAllBatch();
		int max=1;
		for(BatchSetup batch: batchList)
		{
			max  = Math.max(max, batch.getBatchId()+1);
		}
		return max;
	}
	
	@Override
	public String getMatchingPattern(String pattern) {
		
		List<BatchSetupEntity> batchList = batchRepository.findAll();
		boolean ans = false;
		for(BatchSetupEntity entity: batchList)
		{
			String str = entity.getBatchName().toLowerCase();
			if(str.compareTo(pattern.toLowerCase())==0)
				ans=true;
		}
		
		if(ans) {
			return "Yes";
		}
		else {
			return "No";
		}
	}

	@Override
	@Transactional
	public void updateBatch(BatchSetup batch) 
	{
		BatchSetupEntity storedDate = batchRepository.findById(batch.getBatchId()).get();
		batch.setEntryDateTime(storedDate.getEntryDateTime());
		batch.setEntryUserName(storedDate.getEntryUserName());
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		BatchSetupEntity entity = new BatchSetupEntity();
		BeanUtils.copyProperties(batch, entity);
		entity.setLastUpdateDateTime(new Timestamp(System.currentTimeMillis()));
		entity.setLateUpdateUserName(authentication.getName());
		
		
		for(DaySetup day: batch.getDays())
		{
			DaySetupEntity target = new DaySetupEntity();
			BeanUtils.copyProperties(day, target);
			if(day.isSelected())
			{
				entity.addDay(target);
			}
		}
		
		logger.info("Batch Info "+entity);
		batchRepository.save(entity);
	}
	
	@Override
	@Transactional
	public void deleteBatch(BatchSetup batch) 
	{
		BatchSetupEntity entity = batchRepository.findById(batch.getBatchId()).get();
		batchRepository.delete(entity);	
	}
}
