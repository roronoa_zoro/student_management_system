package com.zeroosoft.education.service.Impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.Chapter;
import com.zeroosoft.education.entity.ChapterEntity;
import com.zeroosoft.education.repo.ChapterRepository;
import com.zeroosoft.education.service.ChapterService;

@Service
public class ChapterImpl implements ChapterService {
	
	@Autowired
	private ChapterRepository repo;

	@Override
	public List<Chapter> getAllChapters() {
		// TODO Auto-generated method stub
		List<ChapterEntity> storedChapters = repo.findAll();
		if (CollectionUtils.isNotEmpty(storedChapters)) {
			List<Chapter> chapterList = new ArrayList<Chapter>();
			
			for (ChapterEntity chapterEntity : storedChapters) {
				Chapter dto = new Chapter();
				BeanUtils.copyProperties(chapterEntity, dto);
				chapterList.add(dto);
			}
			return chapterList;
		}else
			return Collections.emptyList();
	}

	@Override
	public Chapter getChapter(int chapterId) {
		// TODO Auto-generated method stub
		ChapterEntity entity = repo.findById(chapterId).get();
		if (entity != null) {
			Chapter dto = new Chapter();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}else
			return null;
	}

	@Override
	public Chapter addChapter(Chapter chapter) {
		// TODO Auto-generated method stub
		System.out.println("chapter: " + chapter);
			ChapterEntity entity = new ChapterEntity();
			BeanUtils.copyProperties(chapter, entity);
			repo.save(entity);
			BeanUtils.copyProperties(entity, chapter);
		return chapter;
	}

	@Override
	public Chapter updateChapter(Chapter chapter) {
		// TODO Auto-generated method stub
		ChapterEntity entity = repo.findById(chapter.getChapterId()).get();
		if (entity != null) {
			ChapterEntity chapterEntity = new ChapterEntity();
			BeanUtils.copyProperties(chapter, chapterEntity);
			repo.save(chapterEntity);
			return chapter;
		}else
			return null;
	}

	@Override
	public void deleteChapter(int chapterId) {
		ChapterEntity chapterEntity = repo.findById(chapterId).get();
		if (chapterEntity != null) {
			repo.deleteById(chapterId);
		}
	}

	@Override
	public boolean duplicationCheck(String chapterName) {
		// TODO Auto-generated method stub
		boolean isPresent;
		ChapterEntity chapterEntity = repo.findByChapterName(chapterName);
		if (chapterEntity == null) {
			isPresent = false;
			return isPresent;
		}else
			return true;
	}

}
