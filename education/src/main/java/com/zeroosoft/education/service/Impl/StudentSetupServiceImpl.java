package com.zeroosoft.education.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.StudentSetup;
import com.zeroosoft.education.entity.StudentSetupEntity;
import com.zeroosoft.education.repo.StudentSetupRepository;
import com.zeroosoft.education.service.StudentSetupService;

@Service
@Transactional
public class StudentSetupServiceImpl implements StudentSetupService
{
	private static final Logger      logger = LoggerFactory.getLogger(StudentSetupServiceImpl.class);
	
	@Autowired StudentSetupRepository studentRepository; 
	
	@Override
	public void addStudentInfo(StudentSetup student) 
	{
		String entryUserName = SecurityContextHolder.getContext().getAuthentication().getName();
		StudentSetupEntity entity = new StudentSetupEntity();	
		BeanUtils.copyProperties(student, entity);
		entity.setEntryDateTime(new Timestamp(System.currentTimeMillis()));
		entity.setEntryUserName(entryUserName);
		
		logger.info("Student Info: "+entity);
		studentRepository.save(entity);
		
	}

	@Override
	public void updateStudentInfo(StudentSetup student) 
	{
		logger.info("Student Info: "+student);
		StudentSetupEntity entity = new StudentSetupEntity();	
		StudentSetup storedData = showStudentInfo(student.getStudentId());
	    BeanUtils.copyProperties(student, entity);
	    entity.setLastUpdateDateTime(new  Timestamp(System.currentTimeMillis()));
	    entity.setLastUpdateUserName(SecurityContextHolder.getContext().getAuthentication().getName());
	    entity.setEntryDateTime(storedData.getEntryDateTime());
		entity.setEntryUserName(storedData.getEntryUserName());
	     studentRepository.save(entity);	
		
	}

	@Override
	public void deleteStudentInfo(String studentId) {
		StudentSetupEntity storedData = studentRepository.findById(studentId).get();	
		studentRepository.delete(storedData);

	}

	@Override
	public StudentSetup showStudentInfo(String studentId) {
		StudentSetupEntity entity = studentRepository.findById(studentId).get();
        if(entity==null)
		  return null;
        else
        {
           StudentSetup dto = new StudentSetup();
           BeanUtils.copyProperties(entity,dto);
           return dto;

        }

	}

	@Override
	public List<StudentSetup> getStudentList() {
		
		List<StudentSetupEntity> storedStudentList = studentRepository.findAll();
		 
		 if(CollectionUtils.isNotEmpty(storedStudentList))
	        {
	            List<StudentSetup> studentSetupList = new ArrayList<>(storedStudentList.size());
	            for (StudentSetupEntity entity : storedStudentList)
	            {
	            	
	            	StudentSetup dto = new StudentSetup();
	                BeanUtils.copyProperties(entity, dto);
	                studentSetupList.add(dto);
	            }
	            return studentSetupList;
	        }
		 else
		 {
			 return Collections.emptyList();
		 }

	}

	@Override
	public boolean checkDuplicateStudentId(String studentId) 
	{
		Optional<StudentSetupEntity> entity = studentRepository.findById(studentId);
		
		if(entity.isPresent())
			return true;
		else
		 return false;
	}

}
