package com.zeroosoft.education.service.Impl;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.zeroosoft.education.dto.ExamSetup;
import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.dto.StudentResult;
import com.zeroosoft.education.entity.ExamSummaryEntity;
import com.zeroosoft.education.entity.StudentResultEntity;
import com.zeroosoft.education.entity.StudentSetupEntity;
import com.zeroosoft.education.repo.StudentResultRepository;
import com.zeroosoft.education.service.StudentResultService;
import com.zeroosoft.education.service.StudentSetupService;

@Service
@Transactional
public class StudentResultServiceImpl implements StudentResultService
{
	private static final Logger      logger = LoggerFactory.getLogger(StudentResultServiceImpl.class);
	
    @Autowired EntityManager entityManager;
    @Autowired StudentResultRepository resultRepository;
    @Autowired StudentSetupService studentService;
    @Autowired PdfReportGenerationServiceImpl reportService;
    @Autowired BatchSetupServiceImpl batchService;
    @Autowired ExamSetupServiceImpl examSetupService;
	
	@Override
	public List<StudentResult> showRegisteredStudentForResult(Integer batchId) 
	{
	    String sql = "select student from StudentSetupEntity student where student.batchId=:batchId";
	    Query query = entityManager.createQuery(sql);
	    query.setParameter("batchId", batchId);
	    List<StudentSetupEntity> storedStudentList = query.getResultList();
	    if(CollectionUtils.isNotEmpty(storedStudentList))
	    {
	    	List<StudentResult> studentResultList = new ArrayList<StudentResult>();
		    for(StudentSetupEntity entity: storedStudentList)
		    {
		    	StudentResult result = new StudentResult();
		    	BeanUtils.copyProperties(entity, result);
		    	result.setBatchId(entity.getBatchId());
		    	result.setExamId(result.getExamId());
		    	studentResultList.add(result);
		    }
		    return studentResultList;
	    }
	    else
	    	return null;
	}
	
	@Override
	public void studentResultSave(StudentResult result)
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    Timestamp currentDateTime = new Timestamp(System.currentTimeMillis());
		String entryUserName = authentication.getName();
		StudentResultEntity entity  = new StudentResultEntity();
		BeanUtils.copyProperties(result, entity);
		entity.setEntryDateTime(currentDateTime);
		entity.setEntryUserName(entryUserName);
		
		logger.info("Result Info: "+entity);
		resultRepository.save(entity);
	}

	@Override
	public List<StudentResult> showResult(ExamSummary examSummary) 
	{
		Long examSummaryId =  examSummary.getExamSummaryId();
		String sql = "select result from StudentResultEntity result where result.examSummaryId=:examSummaryId";
		Query query = entityManager.createQuery(sql);
		query.setParameter("examSummaryId", examSummaryId);
		List<StudentResultEntity> storedResultList = query.getResultList();
		if(CollectionUtils.isEmpty(storedResultList))
		{
			return Collections.emptyList();
		}
		else
		{
			List<StudentResult> resultList = new ArrayList<StudentResult>();
			for(StudentResultEntity result: storedResultList)
			{
				StudentResult dto = new StudentResult();
				String studentName = studentService.showStudentInfo(result.getStudentId()).getStudentName();
				dto.setStudentName(studentName);
				BeanUtils.copyProperties(result, dto);
				resultList.add(dto);
			}
			return resultList;
		}
		
	}

	@Override
	public StudentResult getStudentResultInfo(Long entryId) 
	{
		Optional<StudentResultEntity> resultList = resultRepository.findById(entryId);
		if(resultList.isPresent())
		{
			StudentResult dto = new StudentResult();
			BeanUtils.copyProperties(resultList.get(), dto);
			return dto;
		}
		
		return null;
	}

	@Override
	public void studentResultUpdate(StudentResult storedResult) 
	{
		StudentResultEntity entity  = new StudentResultEntity();
		BeanUtils.copyProperties(storedResult,entity);
		
		StudentResult dto = getStudentResultInfo(storedResult.getEntryId());
		entity.setLastUpdateDateTime(new Timestamp(System.currentTimeMillis()));
		entity.setEntryDateTime(dto.getEntryDateTime());
		entity.setEntryUserName(dto.getEntryUserName());
		entity.setLateUpdateUserName(SecurityContextHolder.getContext().getAuthentication().getName());
		logger.info("Updated Result: "+entity);
		resultRepository.save(entity);
	}
	
	@Override
	public ExamSummary getLastStudentResult()
	{
		String sql = "select examSummary from ExamSummaryEntity examSummary order by examSummary.resultDate desc";
		Query query = entityManager.createQuery(sql);
		List<ExamSummaryEntity> stockEntryList = query.getResultList();
		ExamSummary examSummary = new ExamSummary();
		for(ExamSummaryEntity entity: stockEntryList)
		{
			if(entity.getResultDate()!=null)
			{
				BeanUtils.copyProperties(entity, examSummary);
				break;
			}
		}
		
		return examSummary;
	}

	@Override
	public void showLastResult(ExamSummary examSummary,OutputStream stream) throws DocumentException, IOException 
	{
	   Document document = reportService.createDocumentInfo(stream,"Student Result Publish");	
	   Font f1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL);
	   Font f2 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
	   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
	   String batchName = batchService.showBatchInfo(examSummary.getBatchId()).getBatchName();
	   ExamSetup examSetup = examSetupService.showExamInfo(examSummary.getExamId(),examSummary.getBatchId());
	   String examName  = examSetup.getExamName();
	   
	   PdfPTable table_noborder1 = new PdfPTable(4);
	   table_noborder1.getDefaultCell().setBorder(0);
	   table_noborder1.setWidthPercentage(108f);
	   table_noborder1.addCell(new Phrase("Batch Name:  ", f2));
	   table_noborder1.addCell(new Phrase(batchName, f1));
	   table_noborder1.addCell(new Phrase("Exam Name:  " , f2));
	   table_noborder1.addCell(new Phrase(examName, f1));
	   table_noborder1.addCell(new Phrase("Exam Date:  ", f2));
	   table_noborder1.addCell(new Phrase(dateFormat.format(examSummary.getExamDate()), f1));
	   table_noborder1.addCell(new Phrase("Result Date:  " , f2));
	   table_noborder1.addCell(new Phrase(dateFormat.format(examSummary.getResultDate()), f1));
	   document.add(table_noborder1);
	   
	   List<StudentResult> resultList = showResult(examSummary);
	      
	   PdfPTable pdfPTable = new PdfPTable(5);
	   pdfPTable.setWidths(new float[] { 1f, 2f, 3, 2, 2});
	   pdfPTable.setHeaderRows(1);
	   pdfPTable = reportService.addStudentResultHeader(pdfPTable);
	   Integer count=1;
	   for(StudentResult result: resultList)
	   {
		   pdfPTable = reportService.addStudentResultData(pdfPTable,result,examSetup.getTotalMarks(),count++);
	   }
	   document.add(new Paragraph(new Phrase("\n")));
	   document.add(pdfPTable);
	   document.close();
	   
	}

}
