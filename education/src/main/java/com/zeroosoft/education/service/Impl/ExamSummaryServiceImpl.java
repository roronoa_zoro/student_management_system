package com.zeroosoft.education.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.entity.ExamSummaryEntity;
import com.zeroosoft.education.repo.ExamSummaryRepository;
import com.zeroosoft.education.service.ExamSetupService;
import com.zeroosoft.education.service.ExamSummaryService;

@Service
@Transactional 
public class ExamSummaryServiceImpl implements  ExamSummaryService
{
	private static final Logger      logger = LoggerFactory.getLogger(ExamSummaryServiceImpl.class);
	
	@Autowired ExamSummaryRepository examSummaryRepository;
	@Autowired EntityManager entityManager;
	@Autowired ExamSetupService examSetupservice;

	@Override
	public Long addExamSummary(ExamSummary examInfo) 
	{
	
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	  Timestamp currentDateTime = new Timestamp(System.currentTimeMillis());
	  String entryUserName = authentication.getName();
	  ExamSummaryEntity target = new ExamSummaryEntity();
	  BeanUtils.copyProperties(examInfo, target);
	  target.setEntryDateTime(currentDateTime);
	  target.setEntryUserName(entryUserName);
	  logger.info("Entity: "+target);
	  return examSummaryRepository.save(target).getExamSummaryId();
	 
	}
	
	@Override
	public Boolean examDateFound(Integer batchId,Long examId)
	{
		String sql = "select examSummary from ExamSummaryEntity examSummary where examSummary.batchId=:batchId and "
				+ "examSummary.examId=:examId";
		
		Query query = entityManager.createQuery(sql);
		query.setParameter("batchId", batchId);
		query.setParameter("examId", examId);
		List<ExamSummaryEntity> summaryList = query.getResultList();
		if(summaryList.size()>0)
			return true;
		else
		 return false;
		
	}

	@Override
	public ExamSummary getExamSummaryInfo(Integer batchId, Long examId) 
	{
		String sql = "select examSummary from ExamSummaryEntity examSummary where examSummary.batchId=:batchId and "
				+ "examSummary.examId=:examId";
		Query query = entityManager.createQuery(sql);
		query.setParameter("batchId", batchId);
		query.setParameter("examId", examId);
		List<ExamSummaryEntity> entityList =query.getResultList();
		if(entityList.isEmpty())
			return null;
		else
		{
			ExamSummary dto = new ExamSummary();
			BeanUtils.copyProperties(entityList.get(0), dto);
			return dto;
		}
		
	}

	@Override
	public ExamSummary getExamSummaryBySummaryId(Long examSummaryId) 
	{
		Optional<ExamSummaryEntity> summaryList =examSummaryRepository.findById(examSummaryId);
		if(summaryList.isPresent())
		{
			ExamSummary dto = new ExamSummary();
			BeanUtils.copyProperties(summaryList.get(), dto);
			logger.info("Exam Summary: "+dto);
			return dto;
		}
		return null;
	}

	@Override
	public void updateExamSummary(ExamSummary storedExamSummary) 
	{
		ExamSummaryEntity target = new ExamSummaryEntity();
		BeanUtils.copyProperties(storedExamSummary, target);
		target.setLastUpdateDateTime(new Timestamp(System.currentTimeMillis()));
	    target.setEntryDateTime(storedExamSummary.getEntryDateTime());
	    target.setEntryUserName(storedExamSummary.getEntryUserName());
	    target.setLateUpdateUserName(SecurityContextHolder.getContext().getAuthentication().getName());
		logger.info("Updated Exam Info: "+target);
		examSummaryRepository.save(target);
	}

}
