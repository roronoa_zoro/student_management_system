package com.zeroosoft.education.service.Impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.zeroosoft.education.dto.LectureFile;
import com.zeroosoft.education.entity.LectureFileEntity;
import com.zeroosoft.education.repo.LectureFileRepository;
import com.zeroosoft.education.service.LectureFileUploadService;

@Service
@Transactional
public class LectureFileUploadServiceImpl implements LectureFileUploadService {
	@Autowired private LectureFileRepository lectureFileRepo;
	@Autowired private ServletContext context;

	@Override
	public List<LectureFile> getAllLectureFiles() {
		// TODO Auto-generated method stub
		List<LectureFileEntity> storedLectureList = lectureFileRepo.findAll();
		List<LectureFile> lectures = new ArrayList<LectureFile>(storedLectureList.size());
		
		if (CollectionUtils.isNotEmpty(storedLectureList)) {
			for (LectureFileEntity entity : storedLectureList) {
				LectureFile dto = new LectureFile();
				BeanUtils.copyProperties(entity, dto);
				lectures.add(dto);
			}
			return lectures;
		}else
			return Collections.emptyList();
		
	}

	@Override
	public LectureFile saveUploadFile(LectureFile lectureFile, MultipartFile file) {
		if (file != null) {
			// get orginal uploaded file name 
			String fileName = file.getOriginalFilename();
			// modifiedFileName = base_name + _ + current_milli_seconds + . + file_extension
			String modifiedFileName = FilenameUtils.getBaseName(fileName)+"_"+System.currentTimeMillis()+"."+FilenameUtils.getExtension(fileName);
			// to store file in specific folder, path of the folder should be give 
			// and in /src/main/webapp folder should be created. then 'images' folder of the path
			File storeFile = getFilePath(modifiedFileName, "images");
			if (storeFile !=null) {
				try {
					// store the file 
					FileUtils.writeByteArrayToFile(storeFile, file.getBytes());
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			
			Date date = new Date();
			//SimpleDateFormat df = new SimpleDateFormat();
			lectureFile.setFileName(fileName);
			lectureFile.setModifiedFileName(modifiedFileName);
			lectureFile.setFileExtension(FilenameUtils.getExtension(fileName));
			lectureFile.setFileSize(file.getSize());
			lectureFile.setCreateDate(date);
			
			LectureFileEntity entity = new LectureFileEntity();
			BeanUtils.copyProperties(lectureFile, entity);
			lectureFileRepo.save(entity);
			
			return lectureFile;
		}
		return null;
	}

	private File getFilePath(String modifiedFileName, String path) {
		// file exists or not, if not then create one
		boolean exists = new File(context.getRealPath("/" + path + "/")).exists();
		if (!exists) {
			// new folder created
			new File(context.getRealPath("/" + path + "/")).mkdir();
		}
		// path created 
		String modifiedFilePath = context.getRealPath("/" + path + "/" + File.separator + modifiedFileName);
		File file = new File(modifiedFilePath);
		return file;
	}

	@Override
	public LectureFile updateLectureFile(LectureFile lectureFile, MultipartFile file) {
		if (lectureFile.getRemoveFile() != null) {
			System.out.println("ok");
			File dbFile = new File(context.getRealPath("/images/"+File.separator+ lectureFile.getRemoveFile()));
			dbFile.delete();
		}
		
		if (!file.isEmpty()) {
			if(lectureFile.getRemoveFile() == null) {
				File dbStoreFile = new File(context.getRealPath("/images/"+ File.separator + lectureFile.getModifiedFileName()));
				if (dbStoreFile.exists()) {
					dbStoreFile.delete();
				}
			}
			String fileName = file.getOriginalFilename();
			String modifiedFileName = FilenameUtils.getBaseName(fileName)+"_"+System.currentTimeMillis()+"."+FilenameUtils.getExtension(fileName);
			File storeFile = getFilePath(modifiedFileName, "images");
			if (storeFile !=null) {
				try {
					FileUtils.writeByteArrayToFile(storeFile, file.getBytes());
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			Date date = new Date();
			//SimpleDateFormat df = new SimpleDateFormat();
			lectureFile.setFileName(fileName);
			lectureFile.setModifiedFileName(modifiedFileName);
			lectureFile.setFileExtension(FilenameUtils.getExtension(fileName));
			lectureFile.setFileSize(file.getSize());
			lectureFile.setUpdateDate(date);
			
			LectureFileEntity entity = new LectureFileEntity();
			BeanUtils.copyProperties(lectureFile, entity);
			lectureFileRepo.save(entity);
			return lectureFile;
		}
		return null;
		
	}

	@Override
	public void deleteLectureFile(long id) {
		LectureFileEntity lectureFileEntity = lectureFileRepo.findById(id).get();
		if (lectureFileEntity != null) {
			File dbStoreFile = new File(context.getRealPath("/images/"+ File.separator + lectureFileEntity.getModifiedFileName()));
			if (dbStoreFile.exists()) {
				dbStoreFile.delete();
			}
		}
		lectureFileRepo.deleteById(id);
		
	}

	@Override
	public LectureFile getLectureFile(long id) {
		LectureFileEntity entity = lectureFileRepo.findById(id).get();
		if (entity != null) {
			LectureFile dto = new LectureFile();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}else
			return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
