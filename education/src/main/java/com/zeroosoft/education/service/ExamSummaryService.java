package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.ExamSummary;

public interface ExamSummaryService 
{
	public Long addExamSummary(ExamSummary examInfo);

	public Boolean examDateFound(Integer batchId, Long examId);
	
	public ExamSummary getExamSummaryInfo(Integer batchId, Long examId);
	
	public ExamSummary getExamSummaryBySummaryId(Long examSummaryId);

	public void updateExamSummary(ExamSummary storedExamSummary);

}
