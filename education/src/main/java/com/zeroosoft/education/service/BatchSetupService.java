package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.BatchSetup;

public interface BatchSetupService {

	public void addBatch(BatchSetup batch);
	
	public void updateBatch(BatchSetup batch);
	
	public BatchSetup showBatchInfo(Integer id);
	
	public List<BatchSetup> showAllBatch();
	
	public Integer getAutoIncrementId();

	public String getMatchingPattern(String pattern);

	public void deleteBatch(BatchSetup batch);
}
