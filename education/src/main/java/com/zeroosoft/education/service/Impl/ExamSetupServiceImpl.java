package com.zeroosoft.education.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.dto.ExamSetup;
import com.zeroosoft.education.entity.ExamIdentityEntity;
import com.zeroosoft.education.entity.ExamSetupEntity;
import com.zeroosoft.education.repo.ExamSetupRepository;
import com.zeroosoft.education.service.ExamSetupService;

@Service
@Transactional
public class ExamSetupServiceImpl implements ExamSetupService 
{
	private static final Logger      logger = LoggerFactory.getLogger(ExamSetupServiceImpl.class);
	
	@Autowired ExamSetupRepository examRepository;
	@Autowired EntityManager entityManager;

	@Override
	public void examSave(ExamSetup exam) 
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Timestamp currentDateTime = new Timestamp(System.currentTimeMillis());
		String entryUserName = authentication.getName();
		ExamSetupEntity entity = new ExamSetupEntity();
		ExamIdentityEntity examIdentity = new ExamIdentityEntity(exam.getBatchId(), getLastExamId(exam.getBatchId())+1);
		
		BeanUtils.copyProperties(exam, entity);
		entity.setEntryUserName(entryUserName);
		entity.setEntryDateTime(currentDateTime);
	    entity.setExamIdentity(examIdentity);
	    
	    if(entity.getTotalMarks()==null)
	    	entity.setTotalMarks(0.0d);
		logger.info("ExamSetup Details: "+ entity);
		
		examRepository.save(entity);
	}

	@Override
	public void examUpdate(ExamSetup exam) 
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		ExamSetupEntity entity = new ExamSetupEntity();
		ExamIdentityEntity examIdentity = new ExamIdentityEntity(exam.getBatchId(), exam.getExamId());
		BeanUtils.copyProperties(exam, entity);
		entity.setLastUpdateDateTime(new Timestamp(System.currentTimeMillis()));
		entity.setExamIdentity(examIdentity);
		entity.setLateUpdateUserName(authentication.getName());
		
		System.out.println(exam.getEntryUserName()+" "+exam.getEntryDateTime());
		logger.info("ExamSetup Details: "+ entity);
		examRepository.save(entity);
	}

	@Override
	public void examDelete(ExamSetup exam) 
	{
	   ExamSetupEntity entity = new ExamSetupEntity();
	   BeanUtils.copyProperties(exam, entity);
	   ExamIdentityEntity examIdentity = new ExamIdentityEntity(exam.getBatchId(), exam.getExamId());
	   entity.setExamIdentity(examIdentity);
	   examRepository.delete(entity);
	}

	@Override
	public ExamSetup showExamInfo(Long examId,Integer batchId) 
	{	
		ExamIdentityEntity id = new ExamIdentityEntity(batchId, examId);
		ExamSetupEntity examEntity = examRepository.getOne(id);
		if(examEntity==null)
			return null;
		else
		{
			ExamSetup exam = new ExamSetup();
			BeanUtils.copyProperties(examEntity, exam);
			exam.setBatchId(examEntity.getExamIdentity().getBatchId());
			exam.setExamId(examEntity.getExamIdentity().getExamId());
			return exam;
		}
	}

	@Override
	public List<ExamSetup> showExamList() 
	{
		
		List<ExamSetupEntity> storedExamSetupList  = examRepository.findAll();
		if(CollectionUtils.isNotEmpty(storedExamSetupList))
		{
			List<ExamSetup> examList = new ArrayList<ExamSetup>(storedExamSetupList.size());
			for(ExamSetupEntity examEntity : storedExamSetupList)
			{
				ExamSetup examSetup = new ExamSetup();
				BeanUtils.copyProperties(examEntity, examSetup);
				examSetup.setBatchId(examEntity.getExamIdentity().getBatchId());
				examSetup.setExamId(examEntity.getExamIdentity().getExamId());
				
				examList.add(examSetup);
			}
			return examList;
		}
		else
			return Collections.emptyList();
	}

	@Override
	public Long getLastExamId(Integer batchId) 
	{
		Long examId =0l;
		String sql = "select exam from ExamSetupEntity exam where exam.examIdentity.batchId=:batchId";
		Query query = entityManager.createQuery(sql);
		query.setParameter("batchId", batchId);
		List<ExamSetupEntity> examIdList = query.getResultList();
		logger.info("Size: "+ examIdList.size());
		for(ExamSetupEntity obj: examIdList)
		{
			Long newId = (Long)obj.getExamIdentity().getExamId();
			examId = Math.max(examId, newId);
		}
	   	return examId;
	}
	
	@Override
	public List<ExamSetup> showExamListSearchById(Integer batchId) 
	{
		String sql = "select exam from ExamSetupEntity exam where exam.examIdentity.batchId=:batchId";
		Query query = entityManager.createQuery(sql);
		query.setParameter("batchId", batchId);
		List<ExamSetupEntity> examEntityList = query.getResultList();
		if(CollectionUtils.isNotEmpty(examEntityList))
		{
			List<ExamSetup> examList = new ArrayList<ExamSetup>(examEntityList.size());
			for(ExamSetupEntity examEntity: examEntityList)
			{
				ExamSetup examSetup = new ExamSetup();
				BeanUtils.copyProperties(examEntity, examSetup);
				examSetup.setBatchId(examEntity.getExamIdentity().getBatchId());
				examSetup.setExamId(examEntity.getExamIdentity().getExamId());
				examList.add(examSetup);
			}
			return examList;
		}
		else
			return null;
		
	}

}
