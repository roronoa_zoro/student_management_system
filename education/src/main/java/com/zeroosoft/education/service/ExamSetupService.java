package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.dto.ExamSetup;

public interface ExamSetupService 
{
    public void examSave(ExamSetup exam);
	
	public void examUpdate(ExamSetup exam);
	
	public void examDelete(ExamSetup exam);

	public ExamSetup showExamInfo(Long examId,Integer batchId);
	
	public List<ExamSetup> showExamList();
	
	public Long getLastExamId(Integer batchId);

	public List<ExamSetup> showExamListSearchById(Integer batchId);


}
