package com.zeroosoft.education.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.zeroosoft.education.dto.Role;
import com.zeroosoft.education.dto.User;
import com.zeroosoft.education.entity.UserEntity;


public interface UserService extends UserDetailsService{
    User addUser(User user);
    User getUser(Long userId);
    UserEntity getUser(String username);
    void updateUser(UserEntity user);
    void updateUser(User user);
    void deleteUser(Long userId);
    List<User> getUsers();
    void assignRoleToUser(Role role, User user);
    void updateProfile(User user);
    //List<UserAutocompleteInfo> getMatchingNames(String name);
}
