package com.zeroosoft.education.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.DaySetup;
import com.zeroosoft.education.entity.DaySetupEntity;
import com.zeroosoft.education.repo.DaySetupRepository;
import com.zeroosoft.education.service.DaySetupService;

@Service
public class DaySetupServiceImpl implements DaySetupService{
	
	@Autowired DaySetupRepository repository;

	@Override
	public List<DaySetup> dayList() {
		
		List<DaySetupEntity> storedDayList  = repository.findAll();
		List<DaySetup> dayList = new ArrayList<DaySetup>();
		for(DaySetupEntity day: storedDayList)
		{
			DaySetup dto = new DaySetup();
			dto.setDayId(day.getDayId());
			dto.setDayName(day.getDayName());
			dayList.add(dto);
		}
		return dayList;
	}

}
