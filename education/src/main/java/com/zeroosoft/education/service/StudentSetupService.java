package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.StudentSetup;

public interface StudentSetupService 
{
	public void addStudentInfo(StudentSetup student);
	
	public void updateStudentInfo(StudentSetup student);
	
	public void deleteStudentInfo(String studentId);
	
	public StudentSetup showStudentInfo(String studentId);
	
	public List<StudentSetup> getStudentList();

	public boolean checkDuplicateStudentId(String studentId);

}
