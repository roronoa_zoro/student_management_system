package com.zeroosoft.education.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.itextpdf.text.DocumentException;
import com.zeroosoft.education.dto.ExamSummary;
import com.zeroosoft.education.dto.StudentResult;

public interface StudentResultService 
{
     public List<StudentResult> showRegisteredStudentForResult(Integer batchId);
     
     public List<StudentResult> showResult(ExamSummary examSummary);

     public void studentResultSave(StudentResult result);
     
     public StudentResult getStudentResultInfo(Long entryId);

	 public void studentResultUpdate(StudentResult storedResult);

	 public ExamSummary getLastStudentResult();

	 public void showLastResult(ExamSummary examSummary,OutputStream stream) throws DocumentException, IOException ;
}
