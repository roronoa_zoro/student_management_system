package com.zeroosoft.education.service.Impl;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.zeroosoft.education.dto.StudentResult;
import com.zeroosoft.education.service.StudentSetupService;

import javax.servlet.http.HttpServletRequest;

@Service
public class PdfReportGenerationServiceImpl 
{
	
	@Autowired StudentSetupService studentService;
	
	public Document createDocumentInfo(OutputStream stream,String header) throws DocumentException, IOException 
	{
		Document document = new Document(PageSize.A4);
		Font font = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
		PdfWriter writer=PdfWriter.getInstance(document, stream);
        HeaderFooterPageEvent event = new HeaderFooterPageEvent();
		writer.setPageEvent(event);
        Paragraph para0 = new Paragraph("Student Coaching Center",font);
        font = new Font(Font.FontFamily.TIMES_ROMAN, 15,Font.BOLD);
		para0.setAlignment(Element.ALIGN_CENTER);
		font = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD);
		Paragraph para1 = new Paragraph("Dhaka",font);
		para1.setAlignment(Element.ALIGN_CENTER);
		para1.setSpacingAfter(-1f);
		font = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
		Paragraph para2 = new Paragraph(header,font);
		font = new Font(Font.FontFamily.TIMES_ROMAN, 11,Font.BOLD);
		Font f2 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD);
		para2.setAlignment(Element.ALIGN_CENTER);
		DottedLineSeparator separator = new DottedLineSeparator();
		separator.setPercentage(59500f / 523f);
		Chunk linebreak = new Chunk(separator);
		Image image = null;
		/*try {
			try {
				image = Image.getInstance(getClass().getClassLoader().getResource(LOGO_PATH));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (BadElementException e3) {
			e3.printStackTrace();
		}
		*/
		Paragraph pt = new Paragraph();
/*		pt.add(new Chunk(image, 0, 0));*/
        document.open();
        document.add(para0);
        document.add(pt);
        document.add(para1);
        document.add(para2);
 	    document.add(linebreak);
		return document;
	}

	public PdfPTable addStudentResultHeader(PdfPTable table) 
	{
		try 
		{
			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10f);
			PdfPCell tableCell;
			tableCell = new PdfPCell(new Phrase("SL.", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);

			tableCell = new PdfPCell(new Phrase("Student ID", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);

			tableCell = new PdfPCell(new Phrase("Student Name", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Marks", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);

			tableCell = new PdfPCell(new Phrase("Percentage", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);
			
		} catch (Exception e) {

		}
		return table;

	}

	public PdfPTable addStudentResultData(PdfPTable table,StudentResult result,Double totalMarks,Integer count) 
	{
		try 
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 10f);
			PdfPCell tableCell;
			tableCell = new PdfPCell(new Phrase(count.toString(), headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);

			tableCell = new PdfPCell(new Phrase(result.getStudentId().toString(), headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);
			
           
			tableCell = new PdfPCell(new Phrase(result.getStudentName(), headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase(String.format("%.2f",result.getStudentMarks()), headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);

			Double percentage = result.getStudentMarks()*100;
			percentage=percentage/totalMarks;
			tableCell = new PdfPCell(new Phrase(String.format("%.2f",percentage)+"%", headFont));
			tableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(tableCell);
			
		} catch (Exception e) {

		}
		return table;

	}


}

class HeaderFooterPageEvent  extends PdfPageEventHelper{
	
	   public void onEndPage(PdfWriter writer, Document document) 
	   {
		   Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		   String currentUserName = authentication.getName();
		   LocalDateTime myDateObj = LocalDateTime.now(); 
		   DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"); 
		   String formattedDate = myDateObj.format(myFormatObj);
		   Font f = new Font(Font.FontFamily.TIMES_ROMAN, 8,Font.BOLD);
		   Phrase pline =new Phrase("__________________________________________________________________________________________________________________________________________________________________________________________________________________________",f); 
		   ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, pline, 410, 27, 0);
           Phrase phrase1 = new Phrase(formattedDate + "             "+ currentUserName +"                                       Software Developed by:  CMOSH IT                   Page "+ writer.getCurrentPageNumber(),f);
           ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, phrase1, 70, 10, 0);
	   }
	   
}
	   
	   