package com.zeroosoft.education.service;

import java.util.List;

import javax.validation.Valid;

import com.zeroosoft.education.dto.VideoUpload;

public interface VideoUploadService {
	
	VideoUpload			addVideo(VideoUpload vUpload);
	
	List<VideoUpload> 	getAllVideos();
	
	VideoUpload			getVideo(Long id);

	VideoUpload         UpdateVideo(VideoUpload video) throws Exception;

	void                deleteVideo(long videoId);

	boolean             checkVideoNameDuplication(String videoName);

}
