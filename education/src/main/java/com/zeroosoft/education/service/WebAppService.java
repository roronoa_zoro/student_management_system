package com.zeroosoft.education.service;

import com.zeroosoft.education.dto.PageAttribute;

public interface WebAppService {
	 PageAttribute getPageAttribute(String url);
}
