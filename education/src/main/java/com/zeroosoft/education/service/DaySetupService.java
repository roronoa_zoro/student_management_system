package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.DaySetup;

public interface DaySetupService 
{
	public List<DaySetup> dayList();

}
