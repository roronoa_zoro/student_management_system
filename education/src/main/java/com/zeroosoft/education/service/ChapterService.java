package com.zeroosoft.education.service;

import java.util.List;

import com.zeroosoft.education.dto.Chapter;

public interface ChapterService {
	List<Chapter> 	getAllChapters();
	
	Chapter			getChapter(int chapterId);
	
	Chapter 		addChapter(Chapter chapter);
	
	Chapter			updateChapter(Chapter chapter);
	
	void			deleteChapter(int chapterId);
	
	boolean			duplicationCheck(String chapterName);
}
