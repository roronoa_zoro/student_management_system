package com.zeroosoft.education.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.zeroosoft.education.dto.LectureFile;

public interface LectureFileUploadService {

	List<LectureFile> getAllLectureFiles();

	LectureFile saveUploadFile(LectureFile lectureFile, MultipartFile file);

	LectureFile updateLectureFile(LectureFile lectureFile, MultipartFile file);

	void deleteLectureFile(long id);

	LectureFile getLectureFile(long id);

}
