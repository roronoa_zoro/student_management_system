package com.zeroosoft.education.service.Impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zeroosoft.education.dto.VideoUpload;
import com.zeroosoft.education.entity.VideoUploadEntity;
import com.zeroosoft.education.repo.VideoUploadRepository;
import com.zeroosoft.education.service.VideoUploadService;

@Service
public class VideoUploadServiceImpl implements VideoUploadService {
	
	@Autowired private VideoUploadRepository vuploadRepo;

	@Override
	public VideoUpload addVideo(VideoUpload vUpload) {
		if (vUpload != null) {
			VideoUploadEntity entity = new VideoUploadEntity();
			BeanUtils.copyProperties(vUpload, entity);
			vuploadRepo.save(entity);
			BeanUtils.copyProperties(entity, vUpload);
			return vUpload;
		}else	
			return null;
	}

	@Override
	public List<VideoUpload> getAllVideos() {
		List<VideoUploadEntity> storedVideoUpload = vuploadRepo.findAll();
		if (CollectionUtils.isNotEmpty(storedVideoUpload)) {
			List<VideoUpload> videos = new ArrayList<VideoUpload>();
			for (VideoUploadEntity videoUploadEntity : storedVideoUpload) {
				VideoUpload video = new VideoUpload();
				BeanUtils.copyProperties(videoUploadEntity, video);
				videos.add(video);
			}
			return videos;
		}else
			return Collections.emptyList();
	}

	@Override
	public VideoUpload getVideo(Long id) {
		VideoUploadEntity entity = vuploadRepo.findById(id).get();
		if (entity != null) {
			VideoUpload dto = new VideoUpload();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}else
			return null;
	}

	@Override
	public VideoUpload UpdateVideo(VideoUpload video) throws Exception {
		// TODO Auto-generated method stu
		VideoUploadEntity oldEntity = vuploadRepo.findById(video.getVideoId()).get();
		if (oldEntity == null) {
			throw new Exception();
		}
		
		VideoUploadEntity entity = new VideoUploadEntity();
		BeanUtils.copyProperties(video, entity);
		vuploadRepo.save(entity);
		
		return video;
	}

	@Override
	public void deleteVideo(long videoId) {
		vuploadRepo.deleteById(videoId);
	}

	@Override
	public boolean checkVideoNameDuplication(String videoName) {
		if (vuploadRepo.findByVideoName(videoName) == null) {
			return true;
		}else
			return false;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
