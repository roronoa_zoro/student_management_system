package com.zeroosoft.education.dto;

import java.util.Date;
import java.util.List;

public class StudentResultList 
{
	private List<StudentResult> results;

	public List<StudentResult> getResults() {
		return results;
	}

	public void setResults(List<StudentResult> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "StudentResultList [results=" + results + "]";
	}
	
	

}
