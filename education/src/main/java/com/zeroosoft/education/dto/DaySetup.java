package com.zeroosoft.education.dto;

import com.zeroosoft.education.entity.DaySetupEntity;

public class DaySetup 
{
	private Integer dayId;
	
	private String dayName;
	
	private boolean selected;

	public Integer getDayId() {
		return dayId;
	}

	public void setDayId(Integer dayId) {
		this.dayId = dayId;
	}

	public String getDayName() {
		return dayName;
	}

	public void setDayName(String dayName) {
		this.dayName = dayName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "DaySetup [dayId=" + dayId + ", dayName=" + dayName + ", selected=" + selected + "]";
	}

	public DaySetup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DaySetup(DaySetupEntity dayEntity) 
	{
	    this.dayId = dayEntity.getDayId();
	    this.dayName = dayEntity.getDayName();
	}
	
	
	
	
	

}
