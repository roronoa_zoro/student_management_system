package com.zeroosoft.education.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VideoUpload {
	private long	videoId;
	@NotNull
	private int		levelId;
	@NotNull
	private int 	chapterId;
	
	@NotEmpty
	@Size(min = 3, max = 150)
	private String 	videoName;
	@NotEmpty
	@Size(min = 3, max = 150)
	private String 	videoSrc;
	
	private String 	youtubeSrcId;
	

	public VideoUpload() {
		// TODO Auto-generated constructor stub
	}


	public long getVideoId() {
		return videoId;
	}

	public void setVideoId(long videoId) {
		this.videoId = videoId;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}


	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName.trim();
	}

	public String getVideoSrc() {
		return videoSrc;
	}

	public void setVideoSrc(String videoSrc) {
		this.videoSrc = videoSrc;
	}
	

	public String getYoutubeSrcId() {
		return youtubeSrcId;
	}

	public void setYoutubeSrcId(String youtubeSrcId) {
		this.youtubeSrcId = youtubeSrcId;
	}


	@Override
	public String toString() {
		return "VideoUpload [videoId=" + videoId + ", levelId=" + levelId + ", chapterId=" + chapterId + ", videoName="
				+ videoName + ", videoSrc=" + videoSrc + ", youtubeSrcId=" + youtubeSrcId + "]";
	}
	
	
}
