package com.zeroosoft.education.dto;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ExamSummary 
{
	private Long examSummaryId;
	
	private Long examId;
	
	private Integer batchId;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")  
	private Date examDate;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date resultDate;
	
	private Timestamp entryDateTime;

	private Timestamp		lastUpdateDateTime;

	private String entryUserName;

	private String lateUpdateUserName;
	
	
	public ExamSummary() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ExamSummary(Long examId, Integer batchId, Date examDate, Date resultDate) {
		super();
		this.examId = examId;
		this.batchId = batchId;
		this.examDate = examDate;
		this.resultDate = resultDate;
	}

	public Long getExamSummaryId() {
		return examSummaryId;
	}

	public void setExamSummaryId(Long examSummaryId) {
		this.examSummaryId = examSummaryId;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
	
	public Date getResultDate() {
		return resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}
	

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}

	@Override
	public String toString() {
		return "ExamSummary [examSummaryId=" + examSummaryId + ", examId=" + examId + ", batchId=" + batchId
				+ ", examDate=" + examDate + ", resultDate=" + resultDate + ", entryDateTime=" + entryDateTime
				+ ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName=" + entryUserName
				+ ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}


}
