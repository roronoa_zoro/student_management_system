package com.zeroosoft.education.dto;

public class StudentLevel {
	private int levelId;
	
	private String levelName;

	public StudentLevel(int levelId, String levelName) {
		this.levelId = levelId;
		this.levelName = levelName;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	@Override
	public String toString() {
		return "StudentLevel [levelId=" + levelId + ", levelName=" + levelName + "]";
	}

	
	
}
