package com.zeroosoft.education.dto;

import java.sql.Timestamp;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class StudentSetup 
{
	@NotEmpty
	@Size(min = 3, max = 200)
	private String studentId;
	
	@NotEmpty
	@Size(min = 3, max = 200)
	private String studentName;
	
	private String gender;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	private Date dateOfBirth;
	
	private String DOB;
	
	private Integer batchId;
	
	private String batchName;
	
	private String session;
	
	@NotEmpty
	@Size(min = 3, max = 100)
	private String educationalInstitute;
	
	private String contactNumber;
	
	private String email;
	
	private String fatherName;
	
	private String motherName;
	
	@NotEmpty
	@Size(min = 3, max = 100)
	private String address;
	
	private Timestamp entryDateTime;
	
	private Timestamp		lastUpdateDateTime;
	
	private String entryUserName;
	
	private String lastUpdateUserName;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getEducationalInstitute() {
		return educationalInstitute;
	}

	public void setEducationalInstitute(String educationalInstitute) {
		this.educationalInstitute = educationalInstitute;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLastUpdateUserName() {
		return lastUpdateUserName;
	}

	public void setLastUpdateUserName(String lastUpdateUserName) {
		this.lastUpdateUserName = lastUpdateUserName;
	}

	@Override
	public String toString() {
		return "StudentSetup [studentId=" + studentId + ", studentName=" + studentName + ", gender=" + gender
				+ ", dateOfBirth=" + dateOfBirth + ", DOB=" + DOB + ", batchId=" + batchId + ", batchName=" + batchName
				+ ", session=" + session + ", educationalInstitute=" + educationalInstitute + ", contactNumber="
				+ contactNumber + ", email=" + email + ", fatherName=" + fatherName + ", motherName=" + motherName
				+ ", address=" + address + ", entryDateTime=" + entryDateTime + ", lastUpdateDateTime="
				+ lastUpdateDateTime + ", entryUserName=" + entryUserName + ", lastUpdateUserName=" + lastUpdateUserName
				+ "]";
	}

	
	
}
