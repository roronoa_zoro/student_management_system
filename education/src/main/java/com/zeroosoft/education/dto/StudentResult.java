package com.zeroosoft.education.dto;

import java.sql.Timestamp;
import java.util.Date;

public class StudentResult extends ExamSummary
{
    private Long entryId;
    
    private Long examSummaryId;
    
    private String studentId;
    
	private String studentName;
    
    private Double studentMarks;
    
    private String entryUserName;
    
    private Timestamp entryDateTime;

	public Long getEntryId() {
		return entryId;
	}

	public void setEntryId(Long entryId) {
		this.entryId = entryId;
	}

	public Long getExamSummaryId() {
		return examSummaryId;
	}

	public void setExamSummaryId(Long examSummaryId) {
		this.examSummaryId = examSummaryId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Double getStudentMarks() {
		return studentMarks;
	}

	public void setStudentMarks(Double studentMarks) {
		this.studentMarks = studentMarks;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	@Override
	public String toString() {
		return "StudentResult [entryId=" + entryId + ", examSummaryId=" + examSummaryId + ", studentId=" + studentId
				+ ", studentName=" + studentName + ", studentMarks=" + studentMarks + ", entryUserName=" + entryUserName
				+ ", entryDateTime=" + entryDateTime + "]";
	}

	
}
