package com.zeroosoft.education.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class BatchSetup {

	private Integer batchId;
	
	@NotEmpty
	private String batchName;
	
	private 	String  shiftStart;
	
	private 	String  shiftEnd;
	
	private String session;
	
	private List<DaySetup> days;
	
	private String dayNames;
	
	private Timestamp entryDateTime;
	
	private Timestamp		lastUpdateDateTime;
	
	private String entryUserName;
	
	private String lateUpdateUserName;

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
   
	public List<DaySetup> getDays() {
		return days;
	}

	public void setDays(List<DaySetup> days) {
		this.days = days;
	}

	public String getShiftStart() {
		return shiftStart;
	}

	public void setShiftStart(String shiftStart) {
		this.shiftStart = shiftStart;
	}

	public String getShiftEnd() {
		return shiftEnd;
	}

	public void setShiftEnd(String shiftEnd) {
		this.shiftEnd = shiftEnd;
	}
    
	
	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getDayNames() {
		return dayNames;
	}

	public void setDayNames(String dayNames) {
		this.dayNames = dayNames;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}
	
	public BatchSetup() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "BatchSetup [batchId=" + batchId + ", batchName=" + batchName + ", shiftStart=" + shiftStart
				+ ", shiftEnd=" + shiftEnd + ", session=" + session + ", days=" + days + ", dayNames=" + dayNames
				+ ", entryDateTime=" + entryDateTime + ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName="
				+ entryUserName + ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}

    
	
}
