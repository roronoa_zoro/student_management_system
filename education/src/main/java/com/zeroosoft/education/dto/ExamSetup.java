package com.zeroosoft.education.dto;

import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ExamSetup 
{
	private Long examId;
	
	private Integer batchId;
	
	@NotEmpty
	@Size(min = 3, max = 50)
	private String examName;
	
	@NotEmpty
	@Size(min = 3, max = 50)
	private String syllabus;
	
	private String duration;
	
	private Double totalMarks;
	
	private Timestamp entryDateTime;
	
	private Timestamp		lastUpdateDateTime;
	
	private String entryUserName;
	
	private String lateUpdateUserName;

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Double getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Double totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Timestamp getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Timestamp entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public Timestamp getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Timestamp lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public String getEntryUserName() {
		return entryUserName;
	}

	public void setEntryUserName(String entryUserName) {
		this.entryUserName = entryUserName;
	}

	public String getLateUpdateUserName() {
		return lateUpdateUserName;
	}

	public void setLateUpdateUserName(String lateUpdateUserName) {
		this.lateUpdateUserName = lateUpdateUserName;
	}

	@Override
	public String toString() {
		return "ExamSetup [examId=" + examId + ", batchId=" + batchId + ", examName=" + examName + ", syllabus="
				+ syllabus + ", duration=" + duration + ", totalMarks=" + totalMarks + ", entryDateTime="
				+ entryDateTime + ", lastUpdateDateTime=" + lastUpdateDateTime + ", entryUserName=" + entryUserName
				+ ", lateUpdateUserName=" + lateUpdateUserName + "]";
	}

	

}
