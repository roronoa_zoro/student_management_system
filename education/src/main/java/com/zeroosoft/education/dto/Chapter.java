package com.zeroosoft.education.dto;

import java.util.List;

public class Chapter {
	private int chapterId;
	
	private String chapterName;
	
	private List<VideoUpload> videoUploads;

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName.trim();
	}
	

	public List<VideoUpload> getVideoUploads() {
		return videoUploads;
	}

	public void setVideoUploads(List<VideoUpload> videoUploads) {
		this.videoUploads = videoUploads;
	}

	@Override
	public String toString() {
		return "Chapter [chapterId=" + chapterId + ", chapterName=" + chapterName + ", videoUploads=" + videoUploads
				+ "]";
	}

	
	
}
