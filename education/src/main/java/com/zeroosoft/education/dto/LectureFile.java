package com.zeroosoft.education.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;


public class LectureFile {
	
	private long 		fileId;
	
	private int			levelId;
	
	private String		levelName;
	
	private String 		lectureName;
	
	private String 		fileName;
	
	private String 		modifiedFileName;
	
	private String 		fileExtension;
	
	private long		fileSize;
	
	private String 		createdBy;
	
	@DateTimeFormat(pattern = "DD/MM/YYYY")
	private Date		createDate;
	
	private String 		updatedBy;
	@DateTimeFormat(pattern = "DD/MM/YYYY")
	private Date		updateDate;
	
	private String		removeFile;
	
	
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public int getLevelId() {
		return levelId;
	}
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getLectureName() {
		return lectureName;
	}
	public void setLectureName(String lectureName) {
		this.lectureName = lectureName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getModifiedFileName() {
		return modifiedFileName;
	}
	public void setModifiedFileName(String modifiedFileName) {
		this.modifiedFileName = modifiedFileName;
	}
	public String getFileExtension() {
		return fileExtension;
	}
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemoveFile() {
		return removeFile;
	}
	public void setRemoveFile(String removeFile) {
		this.removeFile = removeFile;
	}
	@Override
	public String toString() {
		return "LectureFile [fileId=" + fileId + ", levelId=" + levelId + ", levelName=" + levelName + ", lectureName="
				+ lectureName + ", fileName=" + fileName + ", modifiedFileName=" + modifiedFileName + ", fileExtension="
				+ fileExtension + ", fileSize=" + fileSize + ", createdBy=" + createdBy + ", createDate=" + createDate
				+ ", updatedBy=" + updatedBy + ", updateDate=" + updateDate + "]";
	}
	
	

}
