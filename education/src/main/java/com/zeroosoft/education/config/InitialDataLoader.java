package com.zeroosoft.education.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.zeroosoft.education.entity.RoleEntity;
import com.zeroosoft.education.entity.UserEntity;
import com.zeroosoft.education.repo.RoleRepository;
import com.zeroosoft.education.repo.UserRepository;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent>{
	
	boolean alreadySetup;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private RoleRepository roleRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		//PermissionEntity viewPermission = createPrivilegeIfNotFound("profile:user:view");
		
		//List<RoleEntity> roles = roleRepo.findAll();
		
		if(roleRepo.findAll().size()==0)
		{
			RoleEntity entity1 = new RoleEntity();
			entity1.setName("admin");
			roleRepo.save(entity1);
			RoleEntity entity2 = new RoleEntity();
			entity2.setName("user");
			roleRepo.save(entity2);
			//RoleEntity role = roleRepo.findByName("user");
			
			Set<RoleEntity> roles =  new HashSet<RoleEntity>();
			
			roles.add(entity1);
			roles.add(entity2);
			UserEntity user = new UserEntity();
			
			user.setPassword(passwordEncoder.encode("admin"));
			user.setUsername("admin");
			user.setEmail("admin@gmail.com");
			user.setRoles(roles);
			user.setEnabled(true);
			userRepo.save(user);    
			
			alreadySetup = true;
		}
		
	}

	/*
	 * private PermissionEntity createPrivilegeIfNotFound(String name) { // TODO
	 * Auto-generated method stub PermissionEntity entity = peromis return null; }
	 */

}
