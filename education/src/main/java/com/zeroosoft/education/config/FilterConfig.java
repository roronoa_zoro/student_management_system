package com.zeroosoft.education.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zeroosoft.education.filter.PageAttributeFilter;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;

@Configuration
public class FilterConfig
{

    @Bean
    public Filter pageAttributeFilter() {
        return new PageAttributeFilter();
    }

    @Bean
    public FilterRegistrationBean<Filter> customFilterRegistration() {
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<Filter>();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(pageAttributeFilter());
        registration.addUrlPatterns("/","*.html");
        return registration;
    }
}
