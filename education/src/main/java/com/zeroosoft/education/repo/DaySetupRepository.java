package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.zeroosoft.education.entity.DaySetupEntity;

public interface DaySetupRepository extends JpaRepository<DaySetupEntity,Integer> {

}
