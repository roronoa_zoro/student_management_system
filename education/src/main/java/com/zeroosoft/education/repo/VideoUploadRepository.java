package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.VideoUploadEntity;

public interface VideoUploadRepository extends JpaRepository<VideoUploadEntity, Long>  {
	VideoUploadEntity findByVideoName(String videoName);
}
