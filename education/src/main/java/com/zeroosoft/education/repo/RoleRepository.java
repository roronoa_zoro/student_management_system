package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.RoleEntity;


public interface RoleRepository extends JpaRepository<RoleEntity, Integer>{
	RoleEntity findByName(String name);
}
