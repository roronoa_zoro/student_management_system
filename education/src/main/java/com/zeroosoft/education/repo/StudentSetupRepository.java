package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zeroosoft.education.entity.StudentSetupEntity;

@Repository
public interface StudentSetupRepository extends JpaRepository<StudentSetupEntity,String> {

}
