package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.LectureFileEntity;

public interface LectureFileRepository extends JpaRepository<LectureFileEntity, Long> {

}
