package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.ChapterEntity;

public interface ChapterRepository extends JpaRepository<ChapterEntity, Integer> {
	
	ChapterEntity findByChapterName(String chapterName);

}
