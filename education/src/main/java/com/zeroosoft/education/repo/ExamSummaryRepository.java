package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.ExamSummaryEntity;

public interface ExamSummaryRepository extends JpaRepository<ExamSummaryEntity,Long>{

}
