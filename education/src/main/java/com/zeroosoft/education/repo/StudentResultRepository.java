package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zeroosoft.education.entity.StudentResultEntity;

@Repository
public interface StudentResultRepository extends JpaRepository<StudentResultEntity,Long> {

}
