package com.zeroosoft.education.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeroosoft.education.entity.ExamIdentityEntity;
import com.zeroosoft.education.entity.ExamSetupEntity;

public interface ExamSetupRepository extends JpaRepository<ExamSetupEntity,ExamIdentityEntity>{

}
